package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type Client struct {
	token *string
}

func NewClient(arn *string) (*Client, error) {
	secret, err := getSecret(arn)
	if err != nil {
		return nil, fmt.Errorf("error retrieving Secret from SecretsManager")
	}
	client := Client{
		token: secret,
	}
	return &client, nil
}

type Status struct {
	Players       int    `json:"players"`
	ServerVersion string `json:"server_version"`
	StartTime     string `json:"start_time"`
	VIP           bool   `json:"vip"`
}

func (c *Client) getStatus() (*Status, error) {
	client, err := NewHTTPClientWithSettings(HTTPClientSettings{
		Connect:          5 * time.Second,
		ExpectContinue:   1 * time.Second,
		IdleConn:         90 * time.Second,
		ConnKeepAlive:    30 * time.Second,
		MaxAllIdleConns:  100,
		MaxHostIdleConns: 10,
		ResponseHeader:   5 * time.Second,
		TLSHandshake:     5 * time.Second,
	})
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("GET", "https://esi.evetech.net/latest/status", nil)
	if err != nil {
		return nil, err
	}
	req.Header = http.Header{
		"Host":          []string{"esi.evetech.net"},
		"Content-Type":  []string{"application/json"},
		"Authorization": []string{fmt.Sprintf("Bearer %s", *c.token)},
	}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode > 299 {
		return nil, fmt.Errorf(res.Status)
	}
	var status Status
	err = json.NewDecoder(res.Body).Decode(&status)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling status: %v", err)
	}
	return &status, nil
}

// An error occured while creating the connection EVE-Online-Wolf-McPhearson.
// Error while communicating to connector: Failed to validate Connection while attempting
// "ValidateCredentials with CustomConnector" with connector failure instanceUrl cannot be
//  null or empty (Service: null; Status Code: 400; Error Code: Client; Request ID: null; Proxy: null)
