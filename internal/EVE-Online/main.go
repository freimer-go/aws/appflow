package main

import (
	// "context"
	// "encoding/json"
	// "fmt"
	// "log"

	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/freimer-go/aws/appflow"
)

type EVEConnector struct{}

// DescribeEntity implements appflow.Connector
func (*EVEConnector) DescribeEntity(request *appflow.DescribeEntityRequest) *appflow.DescribeEntityResponse {
	panic("unimplemented")
}

// ListEntities implements appflow.Connector
func (*EVEConnector) ListEntities(request *appflow.ListEntitiesRequest) *appflow.ListEntitiesResponse {
	panic("unimplemented")
}

// QueryData implements appflow.Connector
func (*EVEConnector) QueryData(request *appflow.QueryDataRequest) *appflow.QueryDataResponse {
	panic("unimplemented")
}

// RetrieveData implements appflow.Connector
func (*EVEConnector) RetrieveData(request *appflow.RetrieveDataRequest) *appflow.RetrieveDataResponse {
	panic("unimplemented")
}

// ValidateConnectorRuntimeSettings implements appflow.Connector
func (*EVEConnector) ValidateConnectorRuntimeSettings(request *appflow.ValidateConnectorRuntimeSettingsRequest) *appflow.ValidateConnectorRuntimeSettingsResponse {
	return &appflow.ValidateConnectorRuntimeSettingsResponse{
		IsSuccess: true,
	}
}

// ValidateCredentials implements appflow.Connector
func (*EVEConnector) ValidateCredentials(request *appflow.ValidateCredentialsRequest) *appflow.ValidateCredentialsResponse {
	c, err := NewClient(&request.Credentials.SecretArn)
	if err != nil {
		return &appflow.ValidateCredentialsResponse{
			IsSuccess: false,
			ErrorDetails: &appflow.ErrorDetails{
				ErrorCode:         appflow.InvalidCredentials,
				ErrorMessage:      fmt.Sprintf("error validating credentials: %v", err),
				RetryAfterSeconds: 0,
			},
		}
	}
	_, err = c.getStatus()
	if err != nil {
		return &appflow.ValidateCredentialsResponse{
			IsSuccess: false,
			ErrorDetails: &appflow.ErrorDetails{
				ErrorCode:         appflow.InvalidCredentials,
				ErrorMessage:      fmt.Sprintf("error validating credentials: %v", err),
				RetryAfterSeconds: 0,
			},
		}
	}
	return &appflow.ValidateCredentialsResponse{
		IsSuccess: true,
	}
}

// WriteData implements appflow.Connector
func (*EVEConnector) WriteData(request *appflow.WriteDataRequest) *appflow.WriteDataResponse {
	panic("unimplemented")
}

func (h *EVEConnector) DescribeConnectorConfiguration(request *appflow.DescribeConnectorConfigurationRequest) *appflow.DescribeConnectorConfigurationResponse {
	return &appflow.DescribeConnectorConfigurationResponse{
		ConnectorOwner:   "Custom Connector Owner",
		ConnectorName:    "EVE Online",
		ConnectorVersion: "v0.0.1",
		ConnectorModes:   []string{"SOURCE"},
		AuthenticationConfig: &appflow.AuthenticationConfig{
			IsOAuth2Supported: true,
			OAuth2Defaults: &appflow.OAuth2Defaults{
				TokenURL:                  []string{"https://login.eveonline.com/v2/oauth/token"},
				LoginURL:                  []string{"https://login.eveonline.com/v2/oauth/authorize/"},
				AuthURL:                   []string{"https://login.eveonline.com/v2/oauth/authorize/"},
				RefreshURL:                []string{"https://login.eveonline.com/v2/oauth/token"},
				OAuth2GrantTypesSupported: []string{"AUTHORIZATION_CODE"},
				OAuthScopes: []string{
					"publicData",
					"esi-calendar.respond_calendar_events.v1",
					"esi-calendar.read_calendar_events.v1",
					"esi-location.read_location.v1",
					"esi-location.read_ship_type.v1",
					"esi-mail.read_mail.v1",
					"esi-skills.read_skills.v1",
					"esi-skills.read_skillqueue.v1",
					"esi-wallet.read_character_wallet.v1",
					"esi-wallet.read_corporation_wallet.v1",
					"esi-search.search_structures.v1",
					"esi-clones.read_clones.v1",
					"esi-characters.read_contacts.v1",
					"esi-universe.read_structures.v1",
					"esi-bookmarks.read_character_bookmarks.v1",
					"esi-killmails.read_killmails.v1",
					"esi-corporations.read_corporation_membership.v1",
					"esi-assets.read_assets.v1",
					"esi-planets.manage_planets.v1",
					"esi-fleets.read_fleet.v1",
					"esi-fittings.read_fittings.v1",
					"esi-markets.structure_markets.v1",
					"esi-corporations.read_structures.v1",
					"esi-characters.read_loyalty.v1",
					"esi-characters.read_opportunities.v1",
					"esi-characters.read_chat_channels.v1",
					"esi-characters.read_medals.v1",
					"esi-characters.read_standings.v1",
					"esi-characters.read_agents_research.v1",
					"esi-industry.read_character_jobs.v1",
					"esi-markets.read_character_orders.v1",
					"esi-characters.read_blueprints.v1",
					"esi-characters.read_corporation_roles.v1",
					"esi-location.read_online.v1",
					"esi-contracts.read_character_contracts.v1",
					"esi-clones.read_implants.v1",
					"esi-characters.read_fatigue.v1",
					"esi-killmails.read_corporation_killmails.v1",
					"esi-wallet.read_corporation_wallets.v1",
					"esi-characters.read_notifications.v1",
					"esi-corporations.read_divisions.v1",
					"esi-corporations.read_contacts.v1",
					"esi-assets.read_corporation_assets.v1",
					"esi-corporations.read_titles.v1",
					"esi-corporations.read_blueprints.v1",
					"esi-bookmarks.read_corporation_bookmarks.v1",
					"esi-contracts.read_corporation_contracts.v1",
					"esi-corporations.read_standings.v1",
					"esi-corporations.read_starbases.v1",
					"esi-industry.read_corporation_jobs.v1",
					"esi-markets.read_corporation_orders.v1",
					"esi-corporations.read_container_logs.v1",
					"esi-industry.read_character_mining.v1",
					"esi-industry.read_corporation_mining.v1",
					"esi-planets.read_customs_offices.v1",
					"esi-corporations.read_facilities.v1",
					"esi-corporations.read_medals.v1",
					"esi-characters.read_titles.v1",
					"esi-alliances.read_contacts.v1",
					"esi-characters.read_fw_stats.v1",
					"esi-corporations.read_fw_stats.v1",
					"esi-characterstats.read.v1",
				},
				RedirectURL: []string{"https://console.aws.amazon.com/appflow/oauth"}}},
		SupportedApiVersions:    []string{"v1"},
		IsSuccess:               true,
		ConnectorRuntimeSetting: []appflow.ConnectorRuntimeSetting{},
		LogoURL:                 "https://www.userlogos.org/files/logos/jumpordie/eve_online_03.png",
		OperatorsSupported: []string{
			"PROJECTION",
			"LESS_THAN",
			"GREATER_THAN",
			"BETWEEN",
			"LESS_THAN_OR_EQUAL_TO",
			"GREATER_THAN_OR_EQUAL_TO",
			"EQUAL_TO",
			"CONTAINS",
			"NOT_EQUAL_TO",
			"ADDITION",
			"SUBTRACTION",
			"MULTIPLICATION",
			"DIVISION",
			"MASK_ALL",
			"MASK_FIRST_N",
			"MASK_LAST_N",
			"VALIDATE_NON_NULL",
			"VALIDATE_NON_ZERO",
			"VALIDATE_NON_NEGATIVE",
			"VALIDATE_NUMERIC",
			"NO_OP",
		},
		TriggerFrequenciesSupported: []string{
			"BYMINUTE",
			"HOURLY",
			"DAILY",
			"WEEKLY",
			"MONTHLY",
			"ONCE",
		},
		SupportedWriteOperations: []string{},
		SupportedTriggerTypes:    []string{"SCHEDULED", "ONDEMAND"},
	}
}

func main() {
	h := appflow.LambdaHandler{}
	h.Connector = &EVEConnector{}
	lambda.StartHandler(&h)
}
