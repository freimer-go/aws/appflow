module connector

go 1.17

require (
	github.com/aws/aws-lambda-go v1.28.0
	github.com/aws/aws-sdk-go v1.43.17
	gitlab.com/freimer-go/aws/appflow v0.0.10
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f
)

require (
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)

replace gitlab.com/freimer-go/aws/appflow => ../../
