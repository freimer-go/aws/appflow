package parser

import (
	"fmt"
	"strings"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

type syntaxErrorListener struct {
	antlr.ErrorListener
	hasError bool
	errors   []string
}

func (l syntaxErrorListener) SyntaxError(recognizer antlr.Recognizer, offendingSymbol interface{}, line, column int, msg string, e antlr.RecognitionException) {
	l.hasError = true
	l.errors = append(l.errors, fmt.Sprintf("filterExpression error line %d:%d at %v:%s", line, column, offendingSymbol, msg))
}

func Parse(filterExpression string) (antlr.Tree, error) {
	// Following based off AWS Python implementation
	is := antlr.NewInputStream(filterExpression)
	lexer := NewCustomConnectorQueryFilterLexer(is)
	stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)
	p := NewCustomConnectorQueryFilterParser(stream)
	p.RemoveErrorListeners()
	errorListener := syntaxErrorListener{}
	p.AddErrorListener(errorListener)
	tree := p.Queryfilter()
	if errorListener.hasError {
		return nil, fmt.Errorf(strings.Join(errorListener.errors, ","))
	}
	return tree, nil
}
