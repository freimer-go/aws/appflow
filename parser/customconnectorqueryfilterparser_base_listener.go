// Code generated from CustomConnectorQueryFilterParser.g4 by ANTLR 4.9.3. DO NOT EDIT.

package parser // CustomConnectorQueryFilterParser

import "github.com/antlr/antlr4/runtime/Go/antlr"

// BaseCustomConnectorQueryFilterParserListener is a complete listener for a parse tree produced by CustomConnectorQueryFilterParser.
type BaseCustomConnectorQueryFilterParserListener struct{}

var _ CustomConnectorQueryFilterParserListener = &BaseCustomConnectorQueryFilterParserListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseCustomConnectorQueryFilterParserListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseCustomConnectorQueryFilterParserListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterQueryfilter is called when production queryfilter is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterQueryfilter(ctx *QueryfilterContext) {}

// ExitQueryfilter is called when production queryfilter is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitQueryfilter(ctx *QueryfilterContext) {}

// EnterLesserThanComparatorExpression is called when production lesserThanComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterLesserThanComparatorExpression(ctx *LesserThanComparatorExpressionContext) {
}

// ExitLesserThanComparatorExpression is called when production lesserThanComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitLesserThanComparatorExpression(ctx *LesserThanComparatorExpressionContext) {
}

// EnterGreaterThanComparatorExpression is called when production greaterThanComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterGreaterThanComparatorExpression(ctx *GreaterThanComparatorExpressionContext) {
}

// ExitGreaterThanComparatorExpression is called when production greaterThanComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitGreaterThanComparatorExpression(ctx *GreaterThanComparatorExpressionContext) {
}

// EnterBooleanEqualToComparatorExpression is called when production booleanEqualToComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterBooleanEqualToComparatorExpression(ctx *BooleanEqualToComparatorExpressionContext) {
}

// ExitBooleanEqualToComparatorExpression is called when production booleanEqualToComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitBooleanEqualToComparatorExpression(ctx *BooleanEqualToComparatorExpressionContext) {
}

// EnterValueExpression is called when production valueExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterValueExpression(ctx *ValueExpressionContext) {
}

// ExitValueExpression is called when production valueExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitValueExpression(ctx *ValueExpressionContext) {
}

// EnterBooleanNotEqualToComparatorExpression is called when production booleanNotEqualToComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterBooleanNotEqualToComparatorExpression(ctx *BooleanNotEqualToComparatorExpressionContext) {
}

// ExitBooleanNotEqualToComparatorExpression is called when production booleanNotEqualToComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitBooleanNotEqualToComparatorExpression(ctx *BooleanNotEqualToComparatorExpressionContext) {
}

// EnterIdentifierExpression is called when production identifierExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterIdentifierExpression(ctx *IdentifierExpressionContext) {
}

// ExitIdentifierExpression is called when production identifierExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitIdentifierExpression(ctx *IdentifierExpressionContext) {
}

// EnterNotExpression is called when production notExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterNotExpression(ctx *NotExpressionContext) {
}

// ExitNotExpression is called when production notExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitNotExpression(ctx *NotExpressionContext) {}

// EnterParenExpression is called when production parenExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterParenExpression(ctx *ParenExpressionContext) {
}

// ExitParenExpression is called when production parenExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitParenExpression(ctx *ParenExpressionContext) {
}

// EnterORBinaryExpression is called when production oRBinaryExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterORBinaryExpression(ctx *ORBinaryExpressionContext) {
}

// ExitORBinaryExpression is called when production oRBinaryExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitORBinaryExpression(ctx *ORBinaryExpressionContext) {
}

// EnterEqualToComparatorExpression is called when production equalToComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterEqualToComparatorExpression(ctx *EqualToComparatorExpressionContext) {
}

// ExitEqualToComparatorExpression is called when production equalToComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitEqualToComparatorExpression(ctx *EqualToComparatorExpressionContext) {
}

// EnterBetweenExpression is called when production betweenExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterBetweenExpression(ctx *BetweenExpressionContext) {
}

// ExitBetweenExpression is called when production betweenExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitBetweenExpression(ctx *BetweenExpressionContext) {
}

// EnterInExpression is called when production inExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterInExpression(ctx *InExpressionContext) {}

// ExitInExpression is called when production inExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitInExpression(ctx *InExpressionContext) {}

// EnterGreaterThanEqualToComparatorExpression is called when production greaterThanEqualToComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterGreaterThanEqualToComparatorExpression(ctx *GreaterThanEqualToComparatorExpressionContext) {
}

// ExitGreaterThanEqualToComparatorExpression is called when production greaterThanEqualToComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitGreaterThanEqualToComparatorExpression(ctx *GreaterThanEqualToComparatorExpressionContext) {
}

// EnterLikeComparatorExpression is called when production likeComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterLikeComparatorExpression(ctx *LikeComparatorExpressionContext) {
}

// ExitLikeComparatorExpression is called when production likeComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitLikeComparatorExpression(ctx *LikeComparatorExpressionContext) {
}

// EnterLesserThanEqualToComparatorExpression is called when production lesserThanEqualToComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterLesserThanEqualToComparatorExpression(ctx *LesserThanEqualToComparatorExpressionContext) {
}

// ExitLesserThanEqualToComparatorExpression is called when production lesserThanEqualToComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitLesserThanEqualToComparatorExpression(ctx *LesserThanEqualToComparatorExpressionContext) {
}

// EnterNotEqualToComparatorExpression is called when production notEqualToComparatorExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterNotEqualToComparatorExpression(ctx *NotEqualToComparatorExpressionContext) {
}

// ExitNotEqualToComparatorExpression is called when production notEqualToComparatorExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitNotEqualToComparatorExpression(ctx *NotEqualToComparatorExpressionContext) {
}

// EnterANDBinaryExpression is called when production aNDBinaryExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterANDBinaryExpression(ctx *ANDBinaryExpressionContext) {
}

// ExitANDBinaryExpression is called when production aNDBinaryExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitANDBinaryExpression(ctx *ANDBinaryExpressionContext) {
}

// EnterGtComparator is called when production gtComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterGtComparator(ctx *GtComparatorContext) {}

// ExitGtComparator is called when production gtComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitGtComparator(ctx *GtComparatorContext) {}

// EnterGeComparator is called when production geComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterGeComparator(ctx *GeComparatorContext) {}

// ExitGeComparator is called when production geComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitGeComparator(ctx *GeComparatorContext) {}

// EnterLtComparator is called when production ltComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterLtComparator(ctx *LtComparatorContext) {}

// ExitLtComparator is called when production ltComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitLtComparator(ctx *LtComparatorContext) {}

// EnterLeComparator is called when production leComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterLeComparator(ctx *LeComparatorContext) {}

// ExitLeComparator is called when production leComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitLeComparator(ctx *LeComparatorContext) {}

// EnterEqComparator is called when production eqComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterEqComparator(ctx *EqComparatorContext) {}

// ExitEqComparator is called when production eqComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitEqComparator(ctx *EqComparatorContext) {}

// EnterNeComparator is called when production neComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterNeComparator(ctx *NeComparatorContext) {}

// ExitNeComparator is called when production neComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitNeComparator(ctx *NeComparatorContext) {}

// EnterLikeComparator is called when production likeComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterLikeComparator(ctx *LikeComparatorContext) {
}

// ExitLikeComparator is called when production likeComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitLikeComparator(ctx *LikeComparatorContext) {
}

// EnterBetweenComparator is called when production betweenComparator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterBetweenComparator(ctx *BetweenComparatorContext) {
}

// ExitBetweenComparator is called when production betweenComparator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitBetweenComparator(ctx *BetweenComparatorContext) {
}

// EnterAndBinary is called when production andBinary is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterAndBinary(ctx *AndBinaryContext) {}

// ExitAndBinary is called when production andBinary is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitAndBinary(ctx *AndBinaryContext) {}

// EnterOrBinary is called when production orBinary is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterOrBinary(ctx *OrBinaryContext) {}

// ExitOrBinary is called when production orBinary is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitOrBinary(ctx *OrBinaryContext) {}

// EnterR_boolean is called when production r_boolean is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterR_boolean(ctx *R_booleanContext) {}

// ExitR_boolean is called when production r_boolean is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitR_boolean(ctx *R_booleanContext) {}

// EnterIdentifier is called when production identifier is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterIdentifier(ctx *IdentifierContext) {}

// ExitIdentifier is called when production identifier is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitIdentifier(ctx *IdentifierContext) {}

// EnterInOperator is called when production inOperator is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterInOperator(ctx *InOperatorContext) {}

// ExitInOperator is called when production inOperator is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitInOperator(ctx *InOperatorContext) {}

// EnterR_string is called when production r_string is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterR_string(ctx *R_stringContext) {}

// ExitR_string is called when production r_string is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitR_string(ctx *R_stringContext) {}

// EnterStringValueExpression is called when production stringValueExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterStringValueExpression(ctx *StringValueExpressionContext) {
}

// ExitStringValueExpression is called when production stringValueExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitStringValueExpression(ctx *StringValueExpressionContext) {
}

// EnterDecimalValueExpression is called when production decimalValueExpression is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterDecimalValueExpression(ctx *DecimalValueExpressionContext) {
}

// ExitDecimalValueExpression is called when production decimalValueExpression is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitDecimalValueExpression(ctx *DecimalValueExpressionContext) {
}

// EnterIsoDate is called when production isoDate is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterIsoDate(ctx *IsoDateContext) {}

// ExitIsoDate is called when production isoDate is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitIsoDate(ctx *IsoDateContext) {}

// EnterIsoDateTime is called when production isoDateTime is entered.
func (s *BaseCustomConnectorQueryFilterParserListener) EnterIsoDateTime(ctx *IsoDateTimeContext) {}

// ExitIsoDateTime is called when production isoDateTime is exited.
func (s *BaseCustomConnectorQueryFilterParserListener) ExitIsoDateTime(ctx *IsoDateTimeContext) {}
