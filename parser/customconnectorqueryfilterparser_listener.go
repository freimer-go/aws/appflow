// Code generated from CustomConnectorQueryFilterParser.g4 by ANTLR 4.9.3. DO NOT EDIT.

package parser // CustomConnectorQueryFilterParser

import "github.com/antlr/antlr4/runtime/Go/antlr"

// CustomConnectorQueryFilterParserListener is a complete listener for a parse tree produced by CustomConnectorQueryFilterParser.
type CustomConnectorQueryFilterParserListener interface {
	antlr.ParseTreeListener

	// EnterQueryfilter is called when entering the queryfilter production.
	EnterQueryfilter(c *QueryfilterContext)

	// EnterLesserThanComparatorExpression is called when entering the lesserThanComparatorExpression production.
	EnterLesserThanComparatorExpression(c *LesserThanComparatorExpressionContext)

	// EnterGreaterThanComparatorExpression is called when entering the greaterThanComparatorExpression production.
	EnterGreaterThanComparatorExpression(c *GreaterThanComparatorExpressionContext)

	// EnterBooleanEqualToComparatorExpression is called when entering the booleanEqualToComparatorExpression production.
	EnterBooleanEqualToComparatorExpression(c *BooleanEqualToComparatorExpressionContext)

	// EnterValueExpression is called when entering the valueExpression production.
	EnterValueExpression(c *ValueExpressionContext)

	// EnterBooleanNotEqualToComparatorExpression is called when entering the booleanNotEqualToComparatorExpression production.
	EnterBooleanNotEqualToComparatorExpression(c *BooleanNotEqualToComparatorExpressionContext)

	// EnterIdentifierExpression is called when entering the identifierExpression production.
	EnterIdentifierExpression(c *IdentifierExpressionContext)

	// EnterNotExpression is called when entering the notExpression production.
	EnterNotExpression(c *NotExpressionContext)

	// EnterParenExpression is called when entering the parenExpression production.
	EnterParenExpression(c *ParenExpressionContext)

	// EnterORBinaryExpression is called when entering the oRBinaryExpression production.
	EnterORBinaryExpression(c *ORBinaryExpressionContext)

	// EnterEqualToComparatorExpression is called when entering the equalToComparatorExpression production.
	EnterEqualToComparatorExpression(c *EqualToComparatorExpressionContext)

	// EnterBetweenExpression is called when entering the betweenExpression production.
	EnterBetweenExpression(c *BetweenExpressionContext)

	// EnterInExpression is called when entering the inExpression production.
	EnterInExpression(c *InExpressionContext)

	// EnterGreaterThanEqualToComparatorExpression is called when entering the greaterThanEqualToComparatorExpression production.
	EnterGreaterThanEqualToComparatorExpression(c *GreaterThanEqualToComparatorExpressionContext)

	// EnterLikeComparatorExpression is called when entering the likeComparatorExpression production.
	EnterLikeComparatorExpression(c *LikeComparatorExpressionContext)

	// EnterLesserThanEqualToComparatorExpression is called when entering the lesserThanEqualToComparatorExpression production.
	EnterLesserThanEqualToComparatorExpression(c *LesserThanEqualToComparatorExpressionContext)

	// EnterNotEqualToComparatorExpression is called when entering the notEqualToComparatorExpression production.
	EnterNotEqualToComparatorExpression(c *NotEqualToComparatorExpressionContext)

	// EnterANDBinaryExpression is called when entering the aNDBinaryExpression production.
	EnterANDBinaryExpression(c *ANDBinaryExpressionContext)

	// EnterGtComparator is called when entering the gtComparator production.
	EnterGtComparator(c *GtComparatorContext)

	// EnterGeComparator is called when entering the geComparator production.
	EnterGeComparator(c *GeComparatorContext)

	// EnterLtComparator is called when entering the ltComparator production.
	EnterLtComparator(c *LtComparatorContext)

	// EnterLeComparator is called when entering the leComparator production.
	EnterLeComparator(c *LeComparatorContext)

	// EnterEqComparator is called when entering the eqComparator production.
	EnterEqComparator(c *EqComparatorContext)

	// EnterNeComparator is called when entering the neComparator production.
	EnterNeComparator(c *NeComparatorContext)

	// EnterLikeComparator is called when entering the likeComparator production.
	EnterLikeComparator(c *LikeComparatorContext)

	// EnterBetweenComparator is called when entering the betweenComparator production.
	EnterBetweenComparator(c *BetweenComparatorContext)

	// EnterAndBinary is called when entering the andBinary production.
	EnterAndBinary(c *AndBinaryContext)

	// EnterOrBinary is called when entering the orBinary production.
	EnterOrBinary(c *OrBinaryContext)

	// EnterR_boolean is called when entering the r_boolean production.
	EnterR_boolean(c *R_booleanContext)

	// EnterIdentifier is called when entering the identifier production.
	EnterIdentifier(c *IdentifierContext)

	// EnterInOperator is called when entering the inOperator production.
	EnterInOperator(c *InOperatorContext)

	// EnterR_string is called when entering the r_string production.
	EnterR_string(c *R_stringContext)

	// EnterStringValueExpression is called when entering the stringValueExpression production.
	EnterStringValueExpression(c *StringValueExpressionContext)

	// EnterDecimalValueExpression is called when entering the decimalValueExpression production.
	EnterDecimalValueExpression(c *DecimalValueExpressionContext)

	// EnterIsoDate is called when entering the isoDate production.
	EnterIsoDate(c *IsoDateContext)

	// EnterIsoDateTime is called when entering the isoDateTime production.
	EnterIsoDateTime(c *IsoDateTimeContext)

	// ExitQueryfilter is called when exiting the queryfilter production.
	ExitQueryfilter(c *QueryfilterContext)

	// ExitLesserThanComparatorExpression is called when exiting the lesserThanComparatorExpression production.
	ExitLesserThanComparatorExpression(c *LesserThanComparatorExpressionContext)

	// ExitGreaterThanComparatorExpression is called when exiting the greaterThanComparatorExpression production.
	ExitGreaterThanComparatorExpression(c *GreaterThanComparatorExpressionContext)

	// ExitBooleanEqualToComparatorExpression is called when exiting the booleanEqualToComparatorExpression production.
	ExitBooleanEqualToComparatorExpression(c *BooleanEqualToComparatorExpressionContext)

	// ExitValueExpression is called when exiting the valueExpression production.
	ExitValueExpression(c *ValueExpressionContext)

	// ExitBooleanNotEqualToComparatorExpression is called when exiting the booleanNotEqualToComparatorExpression production.
	ExitBooleanNotEqualToComparatorExpression(c *BooleanNotEqualToComparatorExpressionContext)

	// ExitIdentifierExpression is called when exiting the identifierExpression production.
	ExitIdentifierExpression(c *IdentifierExpressionContext)

	// ExitNotExpression is called when exiting the notExpression production.
	ExitNotExpression(c *NotExpressionContext)

	// ExitParenExpression is called when exiting the parenExpression production.
	ExitParenExpression(c *ParenExpressionContext)

	// ExitORBinaryExpression is called when exiting the oRBinaryExpression production.
	ExitORBinaryExpression(c *ORBinaryExpressionContext)

	// ExitEqualToComparatorExpression is called when exiting the equalToComparatorExpression production.
	ExitEqualToComparatorExpression(c *EqualToComparatorExpressionContext)

	// ExitBetweenExpression is called when exiting the betweenExpression production.
	ExitBetweenExpression(c *BetweenExpressionContext)

	// ExitInExpression is called when exiting the inExpression production.
	ExitInExpression(c *InExpressionContext)

	// ExitGreaterThanEqualToComparatorExpression is called when exiting the greaterThanEqualToComparatorExpression production.
	ExitGreaterThanEqualToComparatorExpression(c *GreaterThanEqualToComparatorExpressionContext)

	// ExitLikeComparatorExpression is called when exiting the likeComparatorExpression production.
	ExitLikeComparatorExpression(c *LikeComparatorExpressionContext)

	// ExitLesserThanEqualToComparatorExpression is called when exiting the lesserThanEqualToComparatorExpression production.
	ExitLesserThanEqualToComparatorExpression(c *LesserThanEqualToComparatorExpressionContext)

	// ExitNotEqualToComparatorExpression is called when exiting the notEqualToComparatorExpression production.
	ExitNotEqualToComparatorExpression(c *NotEqualToComparatorExpressionContext)

	// ExitANDBinaryExpression is called when exiting the aNDBinaryExpression production.
	ExitANDBinaryExpression(c *ANDBinaryExpressionContext)

	// ExitGtComparator is called when exiting the gtComparator production.
	ExitGtComparator(c *GtComparatorContext)

	// ExitGeComparator is called when exiting the geComparator production.
	ExitGeComparator(c *GeComparatorContext)

	// ExitLtComparator is called when exiting the ltComparator production.
	ExitLtComparator(c *LtComparatorContext)

	// ExitLeComparator is called when exiting the leComparator production.
	ExitLeComparator(c *LeComparatorContext)

	// ExitEqComparator is called when exiting the eqComparator production.
	ExitEqComparator(c *EqComparatorContext)

	// ExitNeComparator is called when exiting the neComparator production.
	ExitNeComparator(c *NeComparatorContext)

	// ExitLikeComparator is called when exiting the likeComparator production.
	ExitLikeComparator(c *LikeComparatorContext)

	// ExitBetweenComparator is called when exiting the betweenComparator production.
	ExitBetweenComparator(c *BetweenComparatorContext)

	// ExitAndBinary is called when exiting the andBinary production.
	ExitAndBinary(c *AndBinaryContext)

	// ExitOrBinary is called when exiting the orBinary production.
	ExitOrBinary(c *OrBinaryContext)

	// ExitR_boolean is called when exiting the r_boolean production.
	ExitR_boolean(c *R_booleanContext)

	// ExitIdentifier is called when exiting the identifier production.
	ExitIdentifier(c *IdentifierContext)

	// ExitInOperator is called when exiting the inOperator production.
	ExitInOperator(c *InOperatorContext)

	// ExitR_string is called when exiting the r_string production.
	ExitR_string(c *R_stringContext)

	// ExitStringValueExpression is called when exiting the stringValueExpression production.
	ExitStringValueExpression(c *StringValueExpressionContext)

	// ExitDecimalValueExpression is called when exiting the decimalValueExpression production.
	ExitDecimalValueExpression(c *DecimalValueExpressionContext)

	// ExitIsoDate is called when exiting the isoDate production.
	ExitIsoDate(c *IsoDateContext)

	// ExitIsoDateTime is called when exiting the isoDateTime production.
	ExitIsoDateTime(c *IsoDateTimeContext)
}
