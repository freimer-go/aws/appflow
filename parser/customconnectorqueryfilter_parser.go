// Code generated from CustomConnectorQueryFilterParser.g4 by ANTLR 4.9.3. DO NOT EDIT.

package parser // CustomConnectorQueryFilterParser

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 29, 153,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4, 18, 9,
	18, 3, 2, 3, 2, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 7, 3, 97, 10, 3, 12, 3, 14, 3, 100, 11, 3, 3, 3, 3, 3, 5, 3, 104, 10,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7, 3, 114, 10, 3, 12,
	3, 14, 3, 117, 11, 3, 3, 4, 3, 4, 3, 5, 3, 5, 3, 6, 3, 6, 3, 7, 3, 7, 3,
	8, 3, 8, 3, 9, 3, 9, 3, 10, 3, 10, 3, 11, 3, 11, 3, 12, 3, 12, 3, 13, 3,
	13, 3, 14, 3, 14, 3, 15, 3, 15, 3, 16, 3, 16, 3, 17, 3, 17, 3, 18, 3, 18,
	3, 18, 3, 18, 5, 18, 151, 10, 18, 3, 18, 2, 3, 4, 19, 2, 4, 6, 8, 10, 12,
	14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 2, 4, 3, 2, 6, 7, 4, 2, 18,
	18, 23, 26, 2, 155, 2, 36, 3, 2, 2, 2, 4, 103, 3, 2, 2, 2, 6, 118, 3, 2,
	2, 2, 8, 120, 3, 2, 2, 2, 10, 122, 3, 2, 2, 2, 12, 124, 3, 2, 2, 2, 14,
	126, 3, 2, 2, 2, 16, 128, 3, 2, 2, 2, 18, 130, 3, 2, 2, 2, 20, 132, 3,
	2, 2, 2, 22, 134, 3, 2, 2, 2, 24, 136, 3, 2, 2, 2, 26, 138, 3, 2, 2, 2,
	28, 140, 3, 2, 2, 2, 30, 142, 3, 2, 2, 2, 32, 144, 3, 2, 2, 2, 34, 150,
	3, 2, 2, 2, 36, 37, 5, 4, 3, 2, 37, 38, 7, 2, 2, 3, 38, 3, 3, 2, 2, 2,
	39, 40, 8, 3, 1, 2, 40, 41, 7, 16, 2, 2, 41, 42, 5, 4, 3, 2, 42, 43, 7,
	17, 2, 2, 43, 104, 3, 2, 2, 2, 44, 45, 7, 5, 2, 2, 45, 104, 5, 4, 3, 18,
	46, 47, 5, 28, 15, 2, 47, 48, 5, 6, 4, 2, 48, 49, 5, 34, 18, 2, 49, 104,
	3, 2, 2, 2, 50, 51, 5, 28, 15, 2, 51, 52, 5, 8, 5, 2, 52, 53, 5, 34, 18,
	2, 53, 104, 3, 2, 2, 2, 54, 55, 5, 28, 15, 2, 55, 56, 5, 10, 6, 2, 56,
	57, 5, 34, 18, 2, 57, 104, 3, 2, 2, 2, 58, 59, 5, 28, 15, 2, 59, 60, 5,
	12, 7, 2, 60, 61, 5, 34, 18, 2, 61, 104, 3, 2, 2, 2, 62, 63, 5, 28, 15,
	2, 63, 64, 5, 14, 8, 2, 64, 65, 5, 34, 18, 2, 65, 104, 3, 2, 2, 2, 66,
	67, 5, 28, 15, 2, 67, 68, 5, 14, 8, 2, 68, 69, 5, 26, 14, 2, 69, 104, 3,
	2, 2, 2, 70, 71, 5, 28, 15, 2, 71, 72, 5, 16, 9, 2, 72, 73, 5, 34, 18,
	2, 73, 104, 3, 2, 2, 2, 74, 75, 5, 28, 15, 2, 75, 76, 5, 16, 9, 2, 76,
	77, 5, 26, 14, 2, 77, 104, 3, 2, 2, 2, 78, 79, 5, 28, 15, 2, 79, 80, 5,
	18, 10, 2, 80, 81, 5, 34, 18, 2, 81, 104, 3, 2, 2, 2, 82, 83, 5, 28, 15,
	2, 83, 84, 5, 20, 11, 2, 84, 85, 5, 34, 18, 2, 85, 86, 5, 22, 12, 2, 86,
	87, 5, 34, 18, 2, 87, 104, 3, 2, 2, 2, 88, 104, 5, 28, 15, 2, 89, 104,
	5, 34, 18, 2, 90, 91, 5, 28, 15, 2, 91, 92, 5, 30, 16, 2, 92, 93, 7, 16,
	2, 2, 93, 98, 5, 34, 18, 2, 94, 95, 7, 20, 2, 2, 95, 97, 5, 34, 18, 2,
	96, 94, 3, 2, 2, 2, 97, 100, 3, 2, 2, 2, 98, 96, 3, 2, 2, 2, 98, 99, 3,
	2, 2, 2, 99, 101, 3, 2, 2, 2, 100, 98, 3, 2, 2, 2, 101, 102, 7, 17, 2,
	2, 102, 104, 3, 2, 2, 2, 103, 39, 3, 2, 2, 2, 103, 44, 3, 2, 2, 2, 103,
	46, 3, 2, 2, 2, 103, 50, 3, 2, 2, 2, 103, 54, 3, 2, 2, 2, 103, 58, 3, 2,
	2, 2, 103, 62, 3, 2, 2, 2, 103, 66, 3, 2, 2, 2, 103, 70, 3, 2, 2, 2, 103,
	74, 3, 2, 2, 2, 103, 78, 3, 2, 2, 2, 103, 82, 3, 2, 2, 2, 103, 88, 3, 2,
	2, 2, 103, 89, 3, 2, 2, 2, 103, 90, 3, 2, 2, 2, 104, 115, 3, 2, 2, 2, 105,
	106, 12, 17, 2, 2, 106, 107, 5, 22, 12, 2, 107, 108, 5, 4, 3, 18, 108,
	114, 3, 2, 2, 2, 109, 110, 12, 16, 2, 2, 110, 111, 5, 24, 13, 2, 111, 112,
	5, 4, 3, 17, 112, 114, 3, 2, 2, 2, 113, 105, 3, 2, 2, 2, 113, 109, 3, 2,
	2, 2, 114, 117, 3, 2, 2, 2, 115, 113, 3, 2, 2, 2, 115, 116, 3, 2, 2, 2,
	116, 5, 3, 2, 2, 2, 117, 115, 3, 2, 2, 2, 118, 119, 7, 8, 2, 2, 119, 7,
	3, 2, 2, 2, 120, 121, 7, 9, 2, 2, 121, 9, 3, 2, 2, 2, 122, 123, 7, 10,
	2, 2, 123, 11, 3, 2, 2, 2, 124, 125, 7, 11, 2, 2, 125, 13, 3, 2, 2, 2,
	126, 127, 7, 12, 2, 2, 127, 15, 3, 2, 2, 2, 128, 129, 7, 13, 2, 2, 129,
	17, 3, 2, 2, 2, 130, 131, 7, 14, 2, 2, 131, 19, 3, 2, 2, 2, 132, 133, 7,
	15, 2, 2, 133, 21, 3, 2, 2, 2, 134, 135, 7, 3, 2, 2, 135, 23, 3, 2, 2,
	2, 136, 137, 7, 4, 2, 2, 137, 25, 3, 2, 2, 2, 138, 139, 9, 2, 2, 2, 139,
	27, 3, 2, 2, 2, 140, 141, 7, 21, 2, 2, 141, 29, 3, 2, 2, 2, 142, 143, 7,
	19, 2, 2, 143, 31, 3, 2, 2, 2, 144, 145, 9, 3, 2, 2, 145, 33, 3, 2, 2,
	2, 146, 151, 5, 32, 17, 2, 147, 151, 7, 22, 2, 2, 148, 151, 7, 28, 2, 2,
	149, 151, 7, 29, 2, 2, 150, 146, 3, 2, 2, 2, 150, 147, 3, 2, 2, 2, 150,
	148, 3, 2, 2, 2, 150, 149, 3, 2, 2, 2, 151, 35, 3, 2, 2, 2, 7, 98, 103,
	113, 115, 150,
}
var literalNames = []string{
	"", "", "", "", "", "", "'>'", "'>='", "'<'", "'<='", "'='", "'!='", "",
	"", "'('", "')'", "'null'", "", "','",
}
var symbolicNames = []string{
	"", "AND", "OR", "NOT", "TRUE", "FALSE", "GT", "GE", "LT", "LE", "EQ",
	"NE", "LIKE", "BETWEEN", "LPAREN", "RPAREN", "NULL", "IN", "COMMA", "IDENTIFIER",
	"DECIMAL", "SINGLE_STRING", "DOUBLE_STRING", "EMPTY_SINGLE_STRING", "EMPTY_DOUBLE_STRING",
	"WS", "DATE", "DATETIME",
}

var ruleNames = []string{
	"queryfilter", "expression", "gtComparator", "geComparator", "ltComparator",
	"leComparator", "eqComparator", "neComparator", "likeComparator", "betweenComparator",
	"andBinary", "orBinary", "r_boolean", "identifier", "inOperator", "r_string",
	"value",
}

type CustomConnectorQueryFilterParser struct {
	*antlr.BaseParser
}

// NewCustomConnectorQueryFilterParser produces a new parser instance for the optional input antlr.TokenStream.
//
// The *CustomConnectorQueryFilterParser instance produced may be reused by calling the SetInputStream method.
// The initial parser configuration is expensive to construct, and the object is not thread-safe;
// however, if used within a Golang sync.Pool, the construction cost amortizes well and the
// objects can be used in a thread-safe manner.
func NewCustomConnectorQueryFilterParser(input antlr.TokenStream) *CustomConnectorQueryFilterParser {
	this := new(CustomConnectorQueryFilterParser)
	deserializer := antlr.NewATNDeserializer(nil)
	deserializedATN := deserializer.DeserializeFromUInt16(parserATN)
	decisionToDFA := make([]*antlr.DFA, len(deserializedATN.DecisionToState))
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "CustomConnectorQueryFilterParser.g4"

	return this
}

// CustomConnectorQueryFilterParser tokens.
const (
	CustomConnectorQueryFilterParserEOF                 = antlr.TokenEOF
	CustomConnectorQueryFilterParserAND                 = 1
	CustomConnectorQueryFilterParserOR                  = 2
	CustomConnectorQueryFilterParserNOT                 = 3
	CustomConnectorQueryFilterParserTRUE                = 4
	CustomConnectorQueryFilterParserFALSE               = 5
	CustomConnectorQueryFilterParserGT                  = 6
	CustomConnectorQueryFilterParserGE                  = 7
	CustomConnectorQueryFilterParserLT                  = 8
	CustomConnectorQueryFilterParserLE                  = 9
	CustomConnectorQueryFilterParserEQ                  = 10
	CustomConnectorQueryFilterParserNE                  = 11
	CustomConnectorQueryFilterParserLIKE                = 12
	CustomConnectorQueryFilterParserBETWEEN             = 13
	CustomConnectorQueryFilterParserLPAREN              = 14
	CustomConnectorQueryFilterParserRPAREN              = 15
	CustomConnectorQueryFilterParserNULL                = 16
	CustomConnectorQueryFilterParserIN                  = 17
	CustomConnectorQueryFilterParserCOMMA               = 18
	CustomConnectorQueryFilterParserIDENTIFIER          = 19
	CustomConnectorQueryFilterParserDECIMAL             = 20
	CustomConnectorQueryFilterParserSINGLE_STRING       = 21
	CustomConnectorQueryFilterParserDOUBLE_STRING       = 22
	CustomConnectorQueryFilterParserEMPTY_SINGLE_STRING = 23
	CustomConnectorQueryFilterParserEMPTY_DOUBLE_STRING = 24
	CustomConnectorQueryFilterParserWS                  = 25
	CustomConnectorQueryFilterParserDATE                = 26
	CustomConnectorQueryFilterParserDATETIME            = 27
)

// CustomConnectorQueryFilterParser rules.
const (
	CustomConnectorQueryFilterParserRULE_queryfilter       = 0
	CustomConnectorQueryFilterParserRULE_expression        = 1
	CustomConnectorQueryFilterParserRULE_gtComparator      = 2
	CustomConnectorQueryFilterParserRULE_geComparator      = 3
	CustomConnectorQueryFilterParserRULE_ltComparator      = 4
	CustomConnectorQueryFilterParserRULE_leComparator      = 5
	CustomConnectorQueryFilterParserRULE_eqComparator      = 6
	CustomConnectorQueryFilterParserRULE_neComparator      = 7
	CustomConnectorQueryFilterParserRULE_likeComparator    = 8
	CustomConnectorQueryFilterParserRULE_betweenComparator = 9
	CustomConnectorQueryFilterParserRULE_andBinary         = 10
	CustomConnectorQueryFilterParserRULE_orBinary          = 11
	CustomConnectorQueryFilterParserRULE_r_boolean         = 12
	CustomConnectorQueryFilterParserRULE_identifier        = 13
	CustomConnectorQueryFilterParserRULE_inOperator        = 14
	CustomConnectorQueryFilterParserRULE_r_string          = 15
	CustomConnectorQueryFilterParserRULE_value             = 16
)

// IQueryfilterContext is an interface to support dynamic dispatch.
type IQueryfilterContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsQueryfilterContext differentiates from other interfaces.
	IsQueryfilterContext()
}

type QueryfilterContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyQueryfilterContext() *QueryfilterContext {
	var p = new(QueryfilterContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_queryfilter
	return p
}

func (*QueryfilterContext) IsQueryfilterContext() {}

func NewQueryfilterContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *QueryfilterContext {
	var p = new(QueryfilterContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_queryfilter

	return p
}

func (s *QueryfilterContext) GetParser() antlr.Parser { return s.parser }

func (s *QueryfilterContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *QueryfilterContext) EOF() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserEOF, 0)
}

func (s *QueryfilterContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *QueryfilterContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *QueryfilterContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterQueryfilter(s)
	}
}

func (s *QueryfilterContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitQueryfilter(s)
	}
}

func (p *CustomConnectorQueryFilterParser) Queryfilter() (localctx IQueryfilterContext) {
	this := p
	_ = this

	localctx = NewQueryfilterContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, CustomConnectorQueryFilterParserRULE_queryfilter)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(34)
		p.expression(0)
	}
	{
		p.SetState(35)
		p.Match(CustomConnectorQueryFilterParserEOF)
	}

	return localctx
}

// IExpressionContext is an interface to support dynamic dispatch.
type IExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsExpressionContext differentiates from other interfaces.
	IsExpressionContext()
}

type ExpressionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyExpressionContext() *ExpressionContext {
	var p = new(ExpressionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_expression
	return p
}

func (*ExpressionContext) IsExpressionContext() {}

func NewExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExpressionContext {
	var p = new(ExpressionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_expression

	return p
}

func (s *ExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *ExpressionContext) CopyFrom(ctx *ExpressionContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *ExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type LesserThanComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    ILtComparatorContext
	right IValueContext
}

func NewLesserThanComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *LesserThanComparatorExpressionContext {
	var p = new(LesserThanComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *LesserThanComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *LesserThanComparatorExpressionContext) GetOp() ILtComparatorContext { return s.op }

func (s *LesserThanComparatorExpressionContext) GetRight() IValueContext { return s.right }

func (s *LesserThanComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *LesserThanComparatorExpressionContext) SetOp(v ILtComparatorContext) { s.op = v }

func (s *LesserThanComparatorExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *LesserThanComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LesserThanComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *LesserThanComparatorExpressionContext) LtComparator() ILtComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ILtComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ILtComparatorContext)
}

func (s *LesserThanComparatorExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *LesserThanComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterLesserThanComparatorExpression(s)
	}
}

func (s *LesserThanComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitLesserThanComparatorExpression(s)
	}
}

type GreaterThanComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    IGtComparatorContext
	right IValueContext
}

func NewGreaterThanComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *GreaterThanComparatorExpressionContext {
	var p = new(GreaterThanComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *GreaterThanComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *GreaterThanComparatorExpressionContext) GetOp() IGtComparatorContext { return s.op }

func (s *GreaterThanComparatorExpressionContext) GetRight() IValueContext { return s.right }

func (s *GreaterThanComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *GreaterThanComparatorExpressionContext) SetOp(v IGtComparatorContext) { s.op = v }

func (s *GreaterThanComparatorExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *GreaterThanComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *GreaterThanComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *GreaterThanComparatorExpressionContext) GtComparator() IGtComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IGtComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IGtComparatorContext)
}

func (s *GreaterThanComparatorExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *GreaterThanComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterGreaterThanComparatorExpression(s)
	}
}

func (s *GreaterThanComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitGreaterThanComparatorExpression(s)
	}
}

type BooleanEqualToComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    IEqComparatorContext
	right IR_booleanContext
}

func NewBooleanEqualToComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BooleanEqualToComparatorExpressionContext {
	var p = new(BooleanEqualToComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BooleanEqualToComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *BooleanEqualToComparatorExpressionContext) GetOp() IEqComparatorContext { return s.op }

func (s *BooleanEqualToComparatorExpressionContext) GetRight() IR_booleanContext { return s.right }

func (s *BooleanEqualToComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *BooleanEqualToComparatorExpressionContext) SetOp(v IEqComparatorContext) { s.op = v }

func (s *BooleanEqualToComparatorExpressionContext) SetRight(v IR_booleanContext) { s.right = v }

func (s *BooleanEqualToComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BooleanEqualToComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *BooleanEqualToComparatorExpressionContext) EqComparator() IEqComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IEqComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IEqComparatorContext)
}

func (s *BooleanEqualToComparatorExpressionContext) R_boolean() IR_booleanContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IR_booleanContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IR_booleanContext)
}

func (s *BooleanEqualToComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterBooleanEqualToComparatorExpression(s)
	}
}

func (s *BooleanEqualToComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitBooleanEqualToComparatorExpression(s)
	}
}

type ValueExpressionContext struct {
	*ExpressionContext
}

func NewValueExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ValueExpressionContext {
	var p = new(ValueExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ValueExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ValueExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *ValueExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterValueExpression(s)
	}
}

func (s *ValueExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitValueExpression(s)
	}
}

type BooleanNotEqualToComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    INeComparatorContext
	right IR_booleanContext
}

func NewBooleanNotEqualToComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BooleanNotEqualToComparatorExpressionContext {
	var p = new(BooleanNotEqualToComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BooleanNotEqualToComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *BooleanNotEqualToComparatorExpressionContext) GetOp() INeComparatorContext { return s.op }

func (s *BooleanNotEqualToComparatorExpressionContext) GetRight() IR_booleanContext { return s.right }

func (s *BooleanNotEqualToComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *BooleanNotEqualToComparatorExpressionContext) SetOp(v INeComparatorContext) { s.op = v }

func (s *BooleanNotEqualToComparatorExpressionContext) SetRight(v IR_booleanContext) { s.right = v }

func (s *BooleanNotEqualToComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BooleanNotEqualToComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *BooleanNotEqualToComparatorExpressionContext) NeComparator() INeComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*INeComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(INeComparatorContext)
}

func (s *BooleanNotEqualToComparatorExpressionContext) R_boolean() IR_booleanContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IR_booleanContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IR_booleanContext)
}

func (s *BooleanNotEqualToComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterBooleanNotEqualToComparatorExpression(s)
	}
}

func (s *BooleanNotEqualToComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitBooleanNotEqualToComparatorExpression(s)
	}
}

type IdentifierExpressionContext struct {
	*ExpressionContext
}

func NewIdentifierExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IdentifierExpressionContext {
	var p = new(IdentifierExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *IdentifierExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *IdentifierExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterIdentifierExpression(s)
	}
}

func (s *IdentifierExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitIdentifierExpression(s)
	}
}

type NotExpressionContext struct {
	*ExpressionContext
}

func NewNotExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NotExpressionContext {
	var p = new(NotExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *NotExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NotExpressionContext) NOT() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserNOT, 0)
}

func (s *NotExpressionContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *NotExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterNotExpression(s)
	}
}

func (s *NotExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitNotExpression(s)
	}
}

type ParenExpressionContext struct {
	*ExpressionContext
}

func NewParenExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ParenExpressionContext {
	var p = new(ParenExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ParenExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ParenExpressionContext) LPAREN() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserLPAREN, 0)
}

func (s *ParenExpressionContext) Expression() IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ParenExpressionContext) RPAREN() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserRPAREN, 0)
}

func (s *ParenExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterParenExpression(s)
	}
}

func (s *ParenExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitParenExpression(s)
	}
}

type ORBinaryExpressionContext struct {
	*ExpressionContext
	left  IExpressionContext
	op    IOrBinaryContext
	right IExpressionContext
}

func NewORBinaryExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ORBinaryExpressionContext {
	var p = new(ORBinaryExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ORBinaryExpressionContext) GetLeft() IExpressionContext { return s.left }

func (s *ORBinaryExpressionContext) GetOp() IOrBinaryContext { return s.op }

func (s *ORBinaryExpressionContext) GetRight() IExpressionContext { return s.right }

func (s *ORBinaryExpressionContext) SetLeft(v IExpressionContext) { s.left = v }

func (s *ORBinaryExpressionContext) SetOp(v IOrBinaryContext) { s.op = v }

func (s *ORBinaryExpressionContext) SetRight(v IExpressionContext) { s.right = v }

func (s *ORBinaryExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ORBinaryExpressionContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *ORBinaryExpressionContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ORBinaryExpressionContext) OrBinary() IOrBinaryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IOrBinaryContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IOrBinaryContext)
}

func (s *ORBinaryExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterORBinaryExpression(s)
	}
}

func (s *ORBinaryExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitORBinaryExpression(s)
	}
}

type EqualToComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    IEqComparatorContext
	right IValueContext
}

func NewEqualToComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *EqualToComparatorExpressionContext {
	var p = new(EqualToComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *EqualToComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *EqualToComparatorExpressionContext) GetOp() IEqComparatorContext { return s.op }

func (s *EqualToComparatorExpressionContext) GetRight() IValueContext { return s.right }

func (s *EqualToComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *EqualToComparatorExpressionContext) SetOp(v IEqComparatorContext) { s.op = v }

func (s *EqualToComparatorExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *EqualToComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EqualToComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *EqualToComparatorExpressionContext) EqComparator() IEqComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IEqComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IEqComparatorContext)
}

func (s *EqualToComparatorExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *EqualToComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterEqualToComparatorExpression(s)
	}
}

func (s *EqualToComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitEqualToComparatorExpression(s)
	}
}

type BetweenExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    IBetweenComparatorContext
	l1    IValueContext
	op1   IAndBinaryContext
	right IValueContext
}

func NewBetweenExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *BetweenExpressionContext {
	var p = new(BetweenExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *BetweenExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *BetweenExpressionContext) GetOp() IBetweenComparatorContext { return s.op }

func (s *BetweenExpressionContext) GetL1() IValueContext { return s.l1 }

func (s *BetweenExpressionContext) GetOp1() IAndBinaryContext { return s.op1 }

func (s *BetweenExpressionContext) GetRight() IValueContext { return s.right }

func (s *BetweenExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *BetweenExpressionContext) SetOp(v IBetweenComparatorContext) { s.op = v }

func (s *BetweenExpressionContext) SetL1(v IValueContext) { s.l1 = v }

func (s *BetweenExpressionContext) SetOp1(v IAndBinaryContext) { s.op1 = v }

func (s *BetweenExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *BetweenExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BetweenExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *BetweenExpressionContext) BetweenComparator() IBetweenComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBetweenComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBetweenComparatorContext)
}

func (s *BetweenExpressionContext) AllValue() []IValueContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IValueContext)(nil)).Elem())
	var tst = make([]IValueContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IValueContext)
		}
	}

	return tst
}

func (s *BetweenExpressionContext) Value(i int) IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *BetweenExpressionContext) AndBinary() IAndBinaryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAndBinaryContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAndBinaryContext)
}

func (s *BetweenExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterBetweenExpression(s)
	}
}

func (s *BetweenExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitBetweenExpression(s)
	}
}

type InExpressionContext struct {
	*ExpressionContext
	op IInOperatorContext
}

func NewInExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *InExpressionContext {
	var p = new(InExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *InExpressionContext) GetOp() IInOperatorContext { return s.op }

func (s *InExpressionContext) SetOp(v IInOperatorContext) { s.op = v }

func (s *InExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *InExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *InExpressionContext) LPAREN() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserLPAREN, 0)
}

func (s *InExpressionContext) AllValue() []IValueContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IValueContext)(nil)).Elem())
	var tst = make([]IValueContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IValueContext)
		}
	}

	return tst
}

func (s *InExpressionContext) Value(i int) IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *InExpressionContext) RPAREN() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserRPAREN, 0)
}

func (s *InExpressionContext) InOperator() IInOperatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IInOperatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IInOperatorContext)
}

func (s *InExpressionContext) AllCOMMA() []antlr.TerminalNode {
	return s.GetTokens(CustomConnectorQueryFilterParserCOMMA)
}

func (s *InExpressionContext) COMMA(i int) antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserCOMMA, i)
}

func (s *InExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterInExpression(s)
	}
}

func (s *InExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitInExpression(s)
	}
}

type GreaterThanEqualToComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    IGeComparatorContext
	right IValueContext
}

func NewGreaterThanEqualToComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *GreaterThanEqualToComparatorExpressionContext {
	var p = new(GreaterThanEqualToComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *GreaterThanEqualToComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *GreaterThanEqualToComparatorExpressionContext) GetOp() IGeComparatorContext { return s.op }

func (s *GreaterThanEqualToComparatorExpressionContext) GetRight() IValueContext { return s.right }

func (s *GreaterThanEqualToComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *GreaterThanEqualToComparatorExpressionContext) SetOp(v IGeComparatorContext) { s.op = v }

func (s *GreaterThanEqualToComparatorExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *GreaterThanEqualToComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *GreaterThanEqualToComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *GreaterThanEqualToComparatorExpressionContext) GeComparator() IGeComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IGeComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IGeComparatorContext)
}

func (s *GreaterThanEqualToComparatorExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *GreaterThanEqualToComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterGreaterThanEqualToComparatorExpression(s)
	}
}

func (s *GreaterThanEqualToComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitGreaterThanEqualToComparatorExpression(s)
	}
}

type LikeComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    ILikeComparatorContext
	right IValueContext
}

func NewLikeComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *LikeComparatorExpressionContext {
	var p = new(LikeComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *LikeComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *LikeComparatorExpressionContext) GetOp() ILikeComparatorContext { return s.op }

func (s *LikeComparatorExpressionContext) GetRight() IValueContext { return s.right }

func (s *LikeComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *LikeComparatorExpressionContext) SetOp(v ILikeComparatorContext) { s.op = v }

func (s *LikeComparatorExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *LikeComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LikeComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *LikeComparatorExpressionContext) LikeComparator() ILikeComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ILikeComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ILikeComparatorContext)
}

func (s *LikeComparatorExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *LikeComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterLikeComparatorExpression(s)
	}
}

func (s *LikeComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitLikeComparatorExpression(s)
	}
}

type LesserThanEqualToComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    ILeComparatorContext
	right IValueContext
}

func NewLesserThanEqualToComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *LesserThanEqualToComparatorExpressionContext {
	var p = new(LesserThanEqualToComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *LesserThanEqualToComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *LesserThanEqualToComparatorExpressionContext) GetOp() ILeComparatorContext { return s.op }

func (s *LesserThanEqualToComparatorExpressionContext) GetRight() IValueContext { return s.right }

func (s *LesserThanEqualToComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *LesserThanEqualToComparatorExpressionContext) SetOp(v ILeComparatorContext) { s.op = v }

func (s *LesserThanEqualToComparatorExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *LesserThanEqualToComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LesserThanEqualToComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *LesserThanEqualToComparatorExpressionContext) LeComparator() ILeComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ILeComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ILeComparatorContext)
}

func (s *LesserThanEqualToComparatorExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *LesserThanEqualToComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterLesserThanEqualToComparatorExpression(s)
	}
}

func (s *LesserThanEqualToComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitLesserThanEqualToComparatorExpression(s)
	}
}

type NotEqualToComparatorExpressionContext struct {
	*ExpressionContext
	left  IIdentifierContext
	op    INeComparatorContext
	right IValueContext
}

func NewNotEqualToComparatorExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *NotEqualToComparatorExpressionContext {
	var p = new(NotEqualToComparatorExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *NotEqualToComparatorExpressionContext) GetLeft() IIdentifierContext { return s.left }

func (s *NotEqualToComparatorExpressionContext) GetOp() INeComparatorContext { return s.op }

func (s *NotEqualToComparatorExpressionContext) GetRight() IValueContext { return s.right }

func (s *NotEqualToComparatorExpressionContext) SetLeft(v IIdentifierContext) { s.left = v }

func (s *NotEqualToComparatorExpressionContext) SetOp(v INeComparatorContext) { s.op = v }

func (s *NotEqualToComparatorExpressionContext) SetRight(v IValueContext) { s.right = v }

func (s *NotEqualToComparatorExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NotEqualToComparatorExpressionContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *NotEqualToComparatorExpressionContext) NeComparator() INeComparatorContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*INeComparatorContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(INeComparatorContext)
}

func (s *NotEqualToComparatorExpressionContext) Value() IValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IValueContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IValueContext)
}

func (s *NotEqualToComparatorExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterNotEqualToComparatorExpression(s)
	}
}

func (s *NotEqualToComparatorExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitNotEqualToComparatorExpression(s)
	}
}

type ANDBinaryExpressionContext struct {
	*ExpressionContext
	left  IExpressionContext
	op    IAndBinaryContext
	right IExpressionContext
}

func NewANDBinaryExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *ANDBinaryExpressionContext {
	var p = new(ANDBinaryExpressionContext)

	p.ExpressionContext = NewEmptyExpressionContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ExpressionContext))

	return p
}

func (s *ANDBinaryExpressionContext) GetLeft() IExpressionContext { return s.left }

func (s *ANDBinaryExpressionContext) GetOp() IAndBinaryContext { return s.op }

func (s *ANDBinaryExpressionContext) GetRight() IExpressionContext { return s.right }

func (s *ANDBinaryExpressionContext) SetLeft(v IExpressionContext) { s.left = v }

func (s *ANDBinaryExpressionContext) SetOp(v IAndBinaryContext) { s.op = v }

func (s *ANDBinaryExpressionContext) SetRight(v IExpressionContext) { s.right = v }

func (s *ANDBinaryExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ANDBinaryExpressionContext) AllExpression() []IExpressionContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IExpressionContext)(nil)).Elem())
	var tst = make([]IExpressionContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IExpressionContext)
		}
	}

	return tst
}

func (s *ANDBinaryExpressionContext) Expression(i int) IExpressionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IExpressionContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ANDBinaryExpressionContext) AndBinary() IAndBinaryContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAndBinaryContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAndBinaryContext)
}

func (s *ANDBinaryExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterANDBinaryExpression(s)
	}
}

func (s *ANDBinaryExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitANDBinaryExpression(s)
	}
}

func (p *CustomConnectorQueryFilterParser) Expression() (localctx IExpressionContext) {
	return p.expression(0)
}

func (p *CustomConnectorQueryFilterParser) expression(_p int) (localctx IExpressionContext) {
	this := p
	_ = this

	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()
	_parentState := p.GetState()
	localctx = NewExpressionContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExpressionContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 2
	p.EnterRecursionRule(localctx, 2, CustomConnectorQueryFilterParserRULE_expression, _p)
	var _la int

	defer func() {
		p.UnrollRecursionContexts(_parentctx)
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(101)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 1, p.GetParserRuleContext()) {
	case 1:
		localctx = NewParenExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx

		{
			p.SetState(38)
			p.Match(CustomConnectorQueryFilterParserLPAREN)
		}
		{
			p.SetState(39)
			p.expression(0)
		}
		{
			p.SetState(40)
			p.Match(CustomConnectorQueryFilterParserRPAREN)
		}

	case 2:
		localctx = NewNotExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(42)
			p.Match(CustomConnectorQueryFilterParserNOT)
		}
		{
			p.SetState(43)
			p.expression(16)
		}

	case 3:
		localctx = NewGreaterThanComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(44)

			var _x = p.Identifier()

			localctx.(*GreaterThanComparatorExpressionContext).left = _x
		}
		{
			p.SetState(45)

			var _x = p.GtComparator()

			localctx.(*GreaterThanComparatorExpressionContext).op = _x
		}
		{
			p.SetState(46)

			var _x = p.Value()

			localctx.(*GreaterThanComparatorExpressionContext).right = _x
		}

	case 4:
		localctx = NewGreaterThanEqualToComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(48)

			var _x = p.Identifier()

			localctx.(*GreaterThanEqualToComparatorExpressionContext).left = _x
		}
		{
			p.SetState(49)

			var _x = p.GeComparator()

			localctx.(*GreaterThanEqualToComparatorExpressionContext).op = _x
		}
		{
			p.SetState(50)

			var _x = p.Value()

			localctx.(*GreaterThanEqualToComparatorExpressionContext).right = _x
		}

	case 5:
		localctx = NewLesserThanComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(52)

			var _x = p.Identifier()

			localctx.(*LesserThanComparatorExpressionContext).left = _x
		}
		{
			p.SetState(53)

			var _x = p.LtComparator()

			localctx.(*LesserThanComparatorExpressionContext).op = _x
		}
		{
			p.SetState(54)

			var _x = p.Value()

			localctx.(*LesserThanComparatorExpressionContext).right = _x
		}

	case 6:
		localctx = NewLesserThanEqualToComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(56)

			var _x = p.Identifier()

			localctx.(*LesserThanEqualToComparatorExpressionContext).left = _x
		}
		{
			p.SetState(57)

			var _x = p.LeComparator()

			localctx.(*LesserThanEqualToComparatorExpressionContext).op = _x
		}
		{
			p.SetState(58)

			var _x = p.Value()

			localctx.(*LesserThanEqualToComparatorExpressionContext).right = _x
		}

	case 7:
		localctx = NewEqualToComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(60)

			var _x = p.Identifier()

			localctx.(*EqualToComparatorExpressionContext).left = _x
		}
		{
			p.SetState(61)

			var _x = p.EqComparator()

			localctx.(*EqualToComparatorExpressionContext).op = _x
		}
		{
			p.SetState(62)

			var _x = p.Value()

			localctx.(*EqualToComparatorExpressionContext).right = _x
		}

	case 8:
		localctx = NewBooleanEqualToComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(64)

			var _x = p.Identifier()

			localctx.(*BooleanEqualToComparatorExpressionContext).left = _x
		}
		{
			p.SetState(65)

			var _x = p.EqComparator()

			localctx.(*BooleanEqualToComparatorExpressionContext).op = _x
		}
		{
			p.SetState(66)

			var _x = p.R_boolean()

			localctx.(*BooleanEqualToComparatorExpressionContext).right = _x
		}

	case 9:
		localctx = NewNotEqualToComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(68)

			var _x = p.Identifier()

			localctx.(*NotEqualToComparatorExpressionContext).left = _x
		}
		{
			p.SetState(69)

			var _x = p.NeComparator()

			localctx.(*NotEqualToComparatorExpressionContext).op = _x
		}
		{
			p.SetState(70)

			var _x = p.Value()

			localctx.(*NotEqualToComparatorExpressionContext).right = _x
		}

	case 10:
		localctx = NewBooleanNotEqualToComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(72)

			var _x = p.Identifier()

			localctx.(*BooleanNotEqualToComparatorExpressionContext).left = _x
		}
		{
			p.SetState(73)

			var _x = p.NeComparator()

			localctx.(*BooleanNotEqualToComparatorExpressionContext).op = _x
		}
		{
			p.SetState(74)

			var _x = p.R_boolean()

			localctx.(*BooleanNotEqualToComparatorExpressionContext).right = _x
		}

	case 11:
		localctx = NewLikeComparatorExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(76)

			var _x = p.Identifier()

			localctx.(*LikeComparatorExpressionContext).left = _x
		}
		{
			p.SetState(77)

			var _x = p.LikeComparator()

			localctx.(*LikeComparatorExpressionContext).op = _x
		}
		{
			p.SetState(78)

			var _x = p.Value()

			localctx.(*LikeComparatorExpressionContext).right = _x
		}

	case 12:
		localctx = NewBetweenExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx

		{
			p.SetState(80)

			var _x = p.Identifier()

			localctx.(*BetweenExpressionContext).left = _x
		}
		{
			p.SetState(81)

			var _x = p.BetweenComparator()

			localctx.(*BetweenExpressionContext).op = _x
		}

		{
			p.SetState(82)

			var _x = p.Value()

			localctx.(*BetweenExpressionContext).l1 = _x
		}
		{
			p.SetState(83)

			var _x = p.AndBinary()

			localctx.(*BetweenExpressionContext).op1 = _x
		}
		{
			p.SetState(84)

			var _x = p.Value()

			localctx.(*BetweenExpressionContext).right = _x
		}

	case 13:
		localctx = NewIdentifierExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(86)
			p.Identifier()
		}

	case 14:
		localctx = NewValueExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(87)
			p.Value()
		}

	case 15:
		localctx = NewInExpressionContext(p, localctx)
		p.SetParserRuleContext(localctx)
		_prevctx = localctx
		{
			p.SetState(88)
			p.Identifier()
		}
		{
			p.SetState(89)

			var _x = p.InOperator()

			localctx.(*InExpressionContext).op = _x
		}
		{
			p.SetState(90)
			p.Match(CustomConnectorQueryFilterParserLPAREN)
		}
		{
			p.SetState(91)
			p.Value()
		}
		p.SetState(96)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == CustomConnectorQueryFilterParserCOMMA {
			{
				p.SetState(92)
				p.Match(CustomConnectorQueryFilterParserCOMMA)
			}
			{
				p.SetState(93)
				p.Value()
			}

			p.SetState(98)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}
		{
			p.SetState(99)
			p.Match(CustomConnectorQueryFilterParserRPAREN)
		}

	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(113)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 3, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(111)
			p.GetErrorHandler().Sync(p)
			switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 2, p.GetParserRuleContext()) {
			case 1:
				localctx = NewANDBinaryExpressionContext(p, NewExpressionContext(p, _parentctx, _parentState))
				localctx.(*ANDBinaryExpressionContext).left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, CustomConnectorQueryFilterParserRULE_expression)
				p.SetState(103)

				if !(p.Precpred(p.GetParserRuleContext(), 15)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 15)", ""))
				}
				{
					p.SetState(104)

					var _x = p.AndBinary()

					localctx.(*ANDBinaryExpressionContext).op = _x
				}
				{
					p.SetState(105)

					var _x = p.expression(16)

					localctx.(*ANDBinaryExpressionContext).right = _x
				}

			case 2:
				localctx = NewORBinaryExpressionContext(p, NewExpressionContext(p, _parentctx, _parentState))
				localctx.(*ORBinaryExpressionContext).left = _prevctx

				p.PushNewRecursionContext(localctx, _startState, CustomConnectorQueryFilterParserRULE_expression)
				p.SetState(107)

				if !(p.Precpred(p.GetParserRuleContext(), 14)) {
					panic(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 14)", ""))
				}
				{
					p.SetState(108)

					var _x = p.OrBinary()

					localctx.(*ORBinaryExpressionContext).op = _x
				}
				{
					p.SetState(109)

					var _x = p.expression(15)

					localctx.(*ORBinaryExpressionContext).right = _x
				}

			}

		}
		p.SetState(115)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 3, p.GetParserRuleContext())
	}

	return localctx
}

// IGtComparatorContext is an interface to support dynamic dispatch.
type IGtComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsGtComparatorContext differentiates from other interfaces.
	IsGtComparatorContext()
}

type GtComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyGtComparatorContext() *GtComparatorContext {
	var p = new(GtComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_gtComparator
	return p
}

func (*GtComparatorContext) IsGtComparatorContext() {}

func NewGtComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *GtComparatorContext {
	var p = new(GtComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_gtComparator

	return p
}

func (s *GtComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *GtComparatorContext) GT() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserGT, 0)
}

func (s *GtComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *GtComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *GtComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterGtComparator(s)
	}
}

func (s *GtComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitGtComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) GtComparator() (localctx IGtComparatorContext) {
	this := p
	_ = this

	localctx = NewGtComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, CustomConnectorQueryFilterParserRULE_gtComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(116)
		p.Match(CustomConnectorQueryFilterParserGT)
	}

	return localctx
}

// IGeComparatorContext is an interface to support dynamic dispatch.
type IGeComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsGeComparatorContext differentiates from other interfaces.
	IsGeComparatorContext()
}

type GeComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyGeComparatorContext() *GeComparatorContext {
	var p = new(GeComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_geComparator
	return p
}

func (*GeComparatorContext) IsGeComparatorContext() {}

func NewGeComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *GeComparatorContext {
	var p = new(GeComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_geComparator

	return p
}

func (s *GeComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *GeComparatorContext) GE() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserGE, 0)
}

func (s *GeComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *GeComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *GeComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterGeComparator(s)
	}
}

func (s *GeComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitGeComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) GeComparator() (localctx IGeComparatorContext) {
	this := p
	_ = this

	localctx = NewGeComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, CustomConnectorQueryFilterParserRULE_geComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(118)
		p.Match(CustomConnectorQueryFilterParserGE)
	}

	return localctx
}

// ILtComparatorContext is an interface to support dynamic dispatch.
type ILtComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsLtComparatorContext differentiates from other interfaces.
	IsLtComparatorContext()
}

type LtComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLtComparatorContext() *LtComparatorContext {
	var p = new(LtComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_ltComparator
	return p
}

func (*LtComparatorContext) IsLtComparatorContext() {}

func NewLtComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LtComparatorContext {
	var p = new(LtComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_ltComparator

	return p
}

func (s *LtComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *LtComparatorContext) LT() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserLT, 0)
}

func (s *LtComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LtComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LtComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterLtComparator(s)
	}
}

func (s *LtComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitLtComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) LtComparator() (localctx ILtComparatorContext) {
	this := p
	_ = this

	localctx = NewLtComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, CustomConnectorQueryFilterParserRULE_ltComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(120)
		p.Match(CustomConnectorQueryFilterParserLT)
	}

	return localctx
}

// ILeComparatorContext is an interface to support dynamic dispatch.
type ILeComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsLeComparatorContext differentiates from other interfaces.
	IsLeComparatorContext()
}

type LeComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLeComparatorContext() *LeComparatorContext {
	var p = new(LeComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_leComparator
	return p
}

func (*LeComparatorContext) IsLeComparatorContext() {}

func NewLeComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LeComparatorContext {
	var p = new(LeComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_leComparator

	return p
}

func (s *LeComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *LeComparatorContext) LE() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserLE, 0)
}

func (s *LeComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LeComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LeComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterLeComparator(s)
	}
}

func (s *LeComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitLeComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) LeComparator() (localctx ILeComparatorContext) {
	this := p
	_ = this

	localctx = NewLeComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, CustomConnectorQueryFilterParserRULE_leComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(122)
		p.Match(CustomConnectorQueryFilterParserLE)
	}

	return localctx
}

// IEqComparatorContext is an interface to support dynamic dispatch.
type IEqComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsEqComparatorContext differentiates from other interfaces.
	IsEqComparatorContext()
}

type EqComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEqComparatorContext() *EqComparatorContext {
	var p = new(EqComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_eqComparator
	return p
}

func (*EqComparatorContext) IsEqComparatorContext() {}

func NewEqComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EqComparatorContext {
	var p = new(EqComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_eqComparator

	return p
}

func (s *EqComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *EqComparatorContext) EQ() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserEQ, 0)
}

func (s *EqComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EqComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EqComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterEqComparator(s)
	}
}

func (s *EqComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitEqComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) EqComparator() (localctx IEqComparatorContext) {
	this := p
	_ = this

	localctx = NewEqComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, CustomConnectorQueryFilterParserRULE_eqComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(124)
		p.Match(CustomConnectorQueryFilterParserEQ)
	}

	return localctx
}

// INeComparatorContext is an interface to support dynamic dispatch.
type INeComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsNeComparatorContext differentiates from other interfaces.
	IsNeComparatorContext()
}

type NeComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyNeComparatorContext() *NeComparatorContext {
	var p = new(NeComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_neComparator
	return p
}

func (*NeComparatorContext) IsNeComparatorContext() {}

func NewNeComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *NeComparatorContext {
	var p = new(NeComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_neComparator

	return p
}

func (s *NeComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *NeComparatorContext) NE() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserNE, 0)
}

func (s *NeComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NeComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *NeComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterNeComparator(s)
	}
}

func (s *NeComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitNeComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) NeComparator() (localctx INeComparatorContext) {
	this := p
	_ = this

	localctx = NewNeComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, CustomConnectorQueryFilterParserRULE_neComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(126)
		p.Match(CustomConnectorQueryFilterParserNE)
	}

	return localctx
}

// ILikeComparatorContext is an interface to support dynamic dispatch.
type ILikeComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsLikeComparatorContext differentiates from other interfaces.
	IsLikeComparatorContext()
}

type LikeComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLikeComparatorContext() *LikeComparatorContext {
	var p = new(LikeComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_likeComparator
	return p
}

func (*LikeComparatorContext) IsLikeComparatorContext() {}

func NewLikeComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LikeComparatorContext {
	var p = new(LikeComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_likeComparator

	return p
}

func (s *LikeComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *LikeComparatorContext) LIKE() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserLIKE, 0)
}

func (s *LikeComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LikeComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LikeComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterLikeComparator(s)
	}
}

func (s *LikeComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitLikeComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) LikeComparator() (localctx ILikeComparatorContext) {
	this := p
	_ = this

	localctx = NewLikeComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, CustomConnectorQueryFilterParserRULE_likeComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(128)
		p.Match(CustomConnectorQueryFilterParserLIKE)
	}

	return localctx
}

// IBetweenComparatorContext is an interface to support dynamic dispatch.
type IBetweenComparatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBetweenComparatorContext differentiates from other interfaces.
	IsBetweenComparatorContext()
}

type BetweenComparatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBetweenComparatorContext() *BetweenComparatorContext {
	var p = new(BetweenComparatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_betweenComparator
	return p
}

func (*BetweenComparatorContext) IsBetweenComparatorContext() {}

func NewBetweenComparatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BetweenComparatorContext {
	var p = new(BetweenComparatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_betweenComparator

	return p
}

func (s *BetweenComparatorContext) GetParser() antlr.Parser { return s.parser }

func (s *BetweenComparatorContext) BETWEEN() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserBETWEEN, 0)
}

func (s *BetweenComparatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BetweenComparatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BetweenComparatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterBetweenComparator(s)
	}
}

func (s *BetweenComparatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitBetweenComparator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) BetweenComparator() (localctx IBetweenComparatorContext) {
	this := p
	_ = this

	localctx = NewBetweenComparatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, CustomConnectorQueryFilterParserRULE_betweenComparator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(130)
		p.Match(CustomConnectorQueryFilterParserBETWEEN)
	}

	return localctx
}

// IAndBinaryContext is an interface to support dynamic dispatch.
type IAndBinaryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsAndBinaryContext differentiates from other interfaces.
	IsAndBinaryContext()
}

type AndBinaryContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyAndBinaryContext() *AndBinaryContext {
	var p = new(AndBinaryContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_andBinary
	return p
}

func (*AndBinaryContext) IsAndBinaryContext() {}

func NewAndBinaryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AndBinaryContext {
	var p = new(AndBinaryContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_andBinary

	return p
}

func (s *AndBinaryContext) GetParser() antlr.Parser { return s.parser }

func (s *AndBinaryContext) AND() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserAND, 0)
}

func (s *AndBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AndBinaryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AndBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterAndBinary(s)
	}
}

func (s *AndBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitAndBinary(s)
	}
}

func (p *CustomConnectorQueryFilterParser) AndBinary() (localctx IAndBinaryContext) {
	this := p
	_ = this

	localctx = NewAndBinaryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, CustomConnectorQueryFilterParserRULE_andBinary)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(132)
		p.Match(CustomConnectorQueryFilterParserAND)
	}

	return localctx
}

// IOrBinaryContext is an interface to support dynamic dispatch.
type IOrBinaryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsOrBinaryContext differentiates from other interfaces.
	IsOrBinaryContext()
}

type OrBinaryContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyOrBinaryContext() *OrBinaryContext {
	var p = new(OrBinaryContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_orBinary
	return p
}

func (*OrBinaryContext) IsOrBinaryContext() {}

func NewOrBinaryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *OrBinaryContext {
	var p = new(OrBinaryContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_orBinary

	return p
}

func (s *OrBinaryContext) GetParser() antlr.Parser { return s.parser }

func (s *OrBinaryContext) OR() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserOR, 0)
}

func (s *OrBinaryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *OrBinaryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *OrBinaryContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterOrBinary(s)
	}
}

func (s *OrBinaryContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitOrBinary(s)
	}
}

func (p *CustomConnectorQueryFilterParser) OrBinary() (localctx IOrBinaryContext) {
	this := p
	_ = this

	localctx = NewOrBinaryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, CustomConnectorQueryFilterParserRULE_orBinary)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(134)
		p.Match(CustomConnectorQueryFilterParserOR)
	}

	return localctx
}

// IR_booleanContext is an interface to support dynamic dispatch.
type IR_booleanContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsR_booleanContext differentiates from other interfaces.
	IsR_booleanContext()
}

type R_booleanContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyR_booleanContext() *R_booleanContext {
	var p = new(R_booleanContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_r_boolean
	return p
}

func (*R_booleanContext) IsR_booleanContext() {}

func NewR_booleanContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *R_booleanContext {
	var p = new(R_booleanContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_r_boolean

	return p
}

func (s *R_booleanContext) GetParser() antlr.Parser { return s.parser }

func (s *R_booleanContext) TRUE() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserTRUE, 0)
}

func (s *R_booleanContext) FALSE() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserFALSE, 0)
}

func (s *R_booleanContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *R_booleanContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *R_booleanContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterR_boolean(s)
	}
}

func (s *R_booleanContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitR_boolean(s)
	}
}

func (p *CustomConnectorQueryFilterParser) R_boolean() (localctx IR_booleanContext) {
	this := p
	_ = this

	localctx = NewR_booleanContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, CustomConnectorQueryFilterParserRULE_r_boolean)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(136)
		_la = p.GetTokenStream().LA(1)

		if !(_la == CustomConnectorQueryFilterParserTRUE || _la == CustomConnectorQueryFilterParserFALSE) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_identifier
	return p
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserIDENTIFIER, 0)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterIdentifier(s)
	}
}

func (s *IdentifierContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitIdentifier(s)
	}
}

func (p *CustomConnectorQueryFilterParser) Identifier() (localctx IIdentifierContext) {
	this := p
	_ = this

	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, CustomConnectorQueryFilterParserRULE_identifier)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(138)
		p.Match(CustomConnectorQueryFilterParserIDENTIFIER)
	}

	return localctx
}

// IInOperatorContext is an interface to support dynamic dispatch.
type IInOperatorContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsInOperatorContext differentiates from other interfaces.
	IsInOperatorContext()
}

type InOperatorContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyInOperatorContext() *InOperatorContext {
	var p = new(InOperatorContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_inOperator
	return p
}

func (*InOperatorContext) IsInOperatorContext() {}

func NewInOperatorContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *InOperatorContext {
	var p = new(InOperatorContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_inOperator

	return p
}

func (s *InOperatorContext) GetParser() antlr.Parser { return s.parser }

func (s *InOperatorContext) IN() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserIN, 0)
}

func (s *InOperatorContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *InOperatorContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *InOperatorContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterInOperator(s)
	}
}

func (s *InOperatorContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitInOperator(s)
	}
}

func (p *CustomConnectorQueryFilterParser) InOperator() (localctx IInOperatorContext) {
	this := p
	_ = this

	localctx = NewInOperatorContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, CustomConnectorQueryFilterParserRULE_inOperator)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(140)
		p.Match(CustomConnectorQueryFilterParserIN)
	}

	return localctx
}

// IR_stringContext is an interface to support dynamic dispatch.
type IR_stringContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsR_stringContext differentiates from other interfaces.
	IsR_stringContext()
}

type R_stringContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyR_stringContext() *R_stringContext {
	var p = new(R_stringContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_r_string
	return p
}

func (*R_stringContext) IsR_stringContext() {}

func NewR_stringContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *R_stringContext {
	var p = new(R_stringContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_r_string

	return p
}

func (s *R_stringContext) GetParser() antlr.Parser { return s.parser }

func (s *R_stringContext) SINGLE_STRING() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserSINGLE_STRING, 0)
}

func (s *R_stringContext) DOUBLE_STRING() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserDOUBLE_STRING, 0)
}

func (s *R_stringContext) EMPTY_DOUBLE_STRING() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserEMPTY_DOUBLE_STRING, 0)
}

func (s *R_stringContext) EMPTY_SINGLE_STRING() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserEMPTY_SINGLE_STRING, 0)
}

func (s *R_stringContext) NULL() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserNULL, 0)
}

func (s *R_stringContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *R_stringContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *R_stringContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterR_string(s)
	}
}

func (s *R_stringContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitR_string(s)
	}
}

func (p *CustomConnectorQueryFilterParser) R_string() (localctx IR_stringContext) {
	this := p
	_ = this

	localctx = NewR_stringContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, CustomConnectorQueryFilterParserRULE_r_string)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(142)
		_la = p.GetTokenStream().LA(1)

		if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<CustomConnectorQueryFilterParserNULL)|(1<<CustomConnectorQueryFilterParserSINGLE_STRING)|(1<<CustomConnectorQueryFilterParserDOUBLE_STRING)|(1<<CustomConnectorQueryFilterParserEMPTY_SINGLE_STRING)|(1<<CustomConnectorQueryFilterParserEMPTY_DOUBLE_STRING))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IValueContext is an interface to support dynamic dispatch.
type IValueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsValueContext differentiates from other interfaces.
	IsValueContext()
}

type ValueContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyValueContext() *ValueContext {
	var p = new(ValueContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_value
	return p
}

func (*ValueContext) IsValueContext() {}

func NewValueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ValueContext {
	var p = new(ValueContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CustomConnectorQueryFilterParserRULE_value

	return p
}

func (s *ValueContext) GetParser() antlr.Parser { return s.parser }

func (s *ValueContext) CopyFrom(ctx *ValueContext) {
	s.BaseParserRuleContext.CopyFrom(ctx.BaseParserRuleContext)
}

func (s *ValueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ValueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

type StringValueExpressionContext struct {
	*ValueContext
}

func NewStringValueExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *StringValueExpressionContext {
	var p = new(StringValueExpressionContext)

	p.ValueContext = NewEmptyValueContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ValueContext))

	return p
}

func (s *StringValueExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringValueExpressionContext) R_string() IR_stringContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IR_stringContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IR_stringContext)
}

func (s *StringValueExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterStringValueExpression(s)
	}
}

func (s *StringValueExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitStringValueExpression(s)
	}
}

type DecimalValueExpressionContext struct {
	*ValueContext
}

func NewDecimalValueExpressionContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *DecimalValueExpressionContext {
	var p = new(DecimalValueExpressionContext)

	p.ValueContext = NewEmptyValueContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ValueContext))

	return p
}

func (s *DecimalValueExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *DecimalValueExpressionContext) DECIMAL() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserDECIMAL, 0)
}

func (s *DecimalValueExpressionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterDecimalValueExpression(s)
	}
}

func (s *DecimalValueExpressionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitDecimalValueExpression(s)
	}
}

type IsoDateContext struct {
	*ValueContext
}

func NewIsoDateContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IsoDateContext {
	var p = new(IsoDateContext)

	p.ValueContext = NewEmptyValueContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ValueContext))

	return p
}

func (s *IsoDateContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IsoDateContext) DATE() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserDATE, 0)
}

func (s *IsoDateContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterIsoDate(s)
	}
}

func (s *IsoDateContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitIsoDate(s)
	}
}

type IsoDateTimeContext struct {
	*ValueContext
}

func NewIsoDateTimeContext(parser antlr.Parser, ctx antlr.ParserRuleContext) *IsoDateTimeContext {
	var p = new(IsoDateTimeContext)

	p.ValueContext = NewEmptyValueContext()
	p.parser = parser
	p.CopyFrom(ctx.(*ValueContext))

	return p
}

func (s *IsoDateTimeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IsoDateTimeContext) DATETIME() antlr.TerminalNode {
	return s.GetToken(CustomConnectorQueryFilterParserDATETIME, 0)
}

func (s *IsoDateTimeContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.EnterIsoDateTime(s)
	}
}

func (s *IsoDateTimeContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CustomConnectorQueryFilterParserListener); ok {
		listenerT.ExitIsoDateTime(s)
	}
}

func (p *CustomConnectorQueryFilterParser) Value() (localctx IValueContext) {
	this := p
	_ = this

	localctx = NewValueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, CustomConnectorQueryFilterParserRULE_value)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(148)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case CustomConnectorQueryFilterParserNULL, CustomConnectorQueryFilterParserSINGLE_STRING, CustomConnectorQueryFilterParserDOUBLE_STRING, CustomConnectorQueryFilterParserEMPTY_SINGLE_STRING, CustomConnectorQueryFilterParserEMPTY_DOUBLE_STRING:
		localctx = NewStringValueExpressionContext(p, localctx)
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(144)
			p.R_string()
		}

	case CustomConnectorQueryFilterParserDECIMAL:
		localctx = NewDecimalValueExpressionContext(p, localctx)
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(145)
			p.Match(CustomConnectorQueryFilterParserDECIMAL)
		}

	case CustomConnectorQueryFilterParserDATE:
		localctx = NewIsoDateContext(p, localctx)
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(146)
			p.Match(CustomConnectorQueryFilterParserDATE)
		}

	case CustomConnectorQueryFilterParserDATETIME:
		localctx = NewIsoDateTimeContext(p, localctx)
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(147)
			p.Match(CustomConnectorQueryFilterParserDATETIME)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

func (p *CustomConnectorQueryFilterParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 1:
		var t *ExpressionContext = nil
		if localctx != nil {
			t = localctx.(*ExpressionContext)
		}
		return p.Expression_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *CustomConnectorQueryFilterParser) Expression_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	this := p
	_ = this

	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 15)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 14)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
