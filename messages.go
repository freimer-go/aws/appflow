package appflow

import (
	"gitlab.com/freimer-go/aws/appflow/errorcode"
)

type DescribeConnectorConfigurationRequest struct {
	Type   string  `json:"type"`
	Locale *string `json:"locale,omitempty"`
}

type OAuth2Defaults struct {
	LoginURL                  []string `json:"loginURL"`
	TokenURL                  []string `json:"tokenURL"`
	AuthURL                   []string `json:"authURL"`
	RefreshURL                []string `json:"refreshURL"`
	OAuth2GrantTypesSupported []string `json:"oAuth2GrantTypesSupported"`
	OAuthScopes               []string `json:"oAuthScopes"`
	RedirectURL               []string `json:"redirectURL"`
}

type AuthParameters struct {
	Key                     string   `json:"key"`
	Required                bool     `json:"required"`
	Label                   string   `json:"label"`
	Description             string   `json:"description"`
	SensitiveField          bool     `json:"sensitiveField"`
	ConnectorSuppliedValues []string `json:"connectorSuppliedValues,omitempty"`
}

type CustomAuthConfig struct {
	AuthenticationType string           `json:"authenticationType"`
	AuthParameters     []AuthParameters `json:"authParameters"`
}

type AuthenticationConfig struct {
	IsBasicAuthSupported  bool               `json:"isBasicAuthSupported"`
	IsApiKeyAuthSupported bool               `json:"isApiKeyAuthSupported"`
	IsOAuth2Supported     bool               `json:"isOAuth2Supported"`
	IsCustomAuthSupported bool               `json:"isCustomAuthSupported"`
	OAuth2Defaults        *OAuth2Defaults    `json:"oAuth2Defaults,omitempty"`
	CustomAuthConfig      []CustomAuthConfig `json:"customAuthConfig,omitempty"`
}

type ConnectorRuntimeSetting struct {
	Key                           string   `json:"key"`
	DataType                      string   `json:"dataType"`
	Required                      bool     `json:"required"`
	Label                         string   `json:"label"`
	Description                   string   `json:"description"`
	Scope                         string   `json:"scope"`
	ConnectorSuppliedValueOptions []string `json:"connectorSuppliedValueOptions,omitempty"`
}

type DescribeConnectorConfigurationResponse struct {
	ConnectorOwner              string                    `json:"connectorOwner"`
	ConnectorName               string                    `json:"connectorName"`
	ConnectorVersion            string                    `json:"connectorVersion"`
	ConnectorModes              []string                  `json:"connectorModes"`
	AuthenticationConfig        *AuthenticationConfig     `json:"authenticationConfig"`
	SupportedApiVersions        []string                  `json:"supportedApiVersions"`
	IsSuccess                   bool                      `json:"isSuccess"`
	ConnectorRuntimeSetting     []ConnectorRuntimeSetting `json:"connectorRuntimeSetting,omitempty"`
	LogoURL                     string                    `json:"logoURL"`
	ErrorDetails                *ErrorDetails             `json:"errorDetails,omitempty"`
	OperatorsSupported          []string                  `json:"operatorsSupported"`
	TriggerFrequenciesSupported []string                  `json:"triggerFrequenciesSupported"`
	SupportedWriteOperations    []string                  `json:"supportedWriteOperations"`
	SupportedTriggerTypes       []string                  `json:"supportedTriggerTypes"`
}

type ErrorDetails struct {
	ErrorCode         errorcode.ErrorCode `json:"errorCode"`
	ErrorMessage      string              `json:"errorMessage"`
	RetryAfterSeconds int                 `json:"retryAfterSeconds"`
}

type ValidateConnectorRuntimeSettingsRequest struct {
	Type                     string                 `json:"type"`
	Scope                    string                 `json:"scope"`
	ConnectorRuntimeSettings map[string]interface{} `json:"connectorRuntimeSettings"`
}

type ValidateConnectorRuntimeSettingsResponse struct {
	IsSuccess    bool          `json:"isSuccess"`
	ErrorDetails *ErrorDetails `json:"errorDetails,omitempty"`
}

type ValidateCredentialsRequest struct {
	Type                    string                 `json:"type"`
	Credentials             Credentials            `json:"credentials"`
	ConnectorRuntimeSetting map[string]interface{} `json:"connectorRuntimeSettings,omitempty"`
}

type ValidateCredentialsResponse struct {
	IsSuccess    bool          `json:"isSuccess"`
	ErrorDetails *ErrorDetails `json:"errorDetails,omitempty"`
}

type QueryDataRequest struct {
	Type               string           `json:"type"`
	EntityIdentifier   string           `json:"entityIdentifier"`
	ConnectorContext   ConnectorContext `json:"connectorContext"`
	SelectedFieldNames []string         `json:"selectedFieldNames"`
	FilterExpression   string           `json:"filterExpression"`
	NextToken          string           `json:"nextToken"`
	MaxResults         int              `json:"maxResults"`
}

type Credentials struct {
	SecretArn          string `json:"secretArn"`
	AuthenticationType string `json:"authenticationType"`
}

type Entity struct {
	EntityIdentifier  string `json:"entityIdentifier"`
	HasNestedEntities bool   `json:"hasNestedEntities"`
	Label             string `json:"label"`
	Description       string `json:"description"`
}

type AllowedLengthRange struct {
	MinRange int `json:"minRange"`
	MaxRange int `json:"maxRange"`
}

type Constraints struct {
	AllowedLengthRange        AllowedLengthRange `json:"allowedLengthRange"`
	AllowedValues             []string           `json:"allowedValues"`
	AllowedValuesRegexPattern string             `json:"allowedValuesRegexPattern"`
}

type ReadProperties struct {
	IsRetrievable                         bool `json:"isRetrievable"`
	IsNullable                            bool `json:"isNullable"`
	IsQueryable                           bool `json:"isQueryable"`
	IsTimestampFieldForIncrementalQueries bool `json:"isTimestampFieldForIncrementalQueries"`
}

type WriteProperties struct {
	IsCreatable              bool     `json:"isCreatable"`
	IsUpdatable              bool     `json:"isUpdatable"`
	IsNullable               bool     `json:"isNullable"`
	IsUpsertable             bool     `json:"isUpsertable"`
	SupportedWriteOperations []string `json:"supportedWriteOperations"`
}

type CustomProperties struct {
	CustomProperty string `json:"customProperty"`
}

type Fields struct {
	FieldName        string           `json:"fieldName"`
	DataType         string           `json:"dataType"`
	Label            string           `json:"label"`
	Description      string           `json:"description"`
	IsPrimaryKey     bool             `json:"isPrimaryKey"`
	DefaultValue     string           `json:"defaultValue"`
	IsDeprecated     bool             `json:"isDeprecated"`
	Constraints      Constraints      `json:"constraints"`
	ReadProperties   ReadProperties   `json:"readProperties"`
	WriteProperties  WriteProperties  `json:"writeProperties"`
	CustomProperties CustomProperties `json:"customProperties"`
	FilterOperators  []string         `json:"filterOperators,omitempty"`
}

type EntityDefinition struct {
	Entity           *Entity          `json:"entity"`
	Fields           []Fields         `json:"fields"`
	CustomProperties CustomProperties `json:"customProperties"`
}

type ConnectorContext struct {
	ConnectorRuntimeSettings map[string]interface{} `json:"connectorRuntimeSettings"`
	Credentials              Credentials            `json:"credentials"`
	EntityDefinition         EntityDefinition       `json:"entityDefinition"`
	APIVersion               string                 `json:"apiVersion"`
}

type QueryDataResponse struct {
	IsSuccess    bool          `json:"isSuccess"`
	ErrorDetails *ErrorDetails `json:"errorDetails,omitempty"`
	NextToken    string        `json:"nextToken"`
	Records      []string      `json:"records"`
}

type ListEntitiesRequest struct {
	Type             string           `json:"type"`
	EntitiesPath     string           `json:"entitiesPath"`
	MaxResult        int              `json:"maxResult"`
	NextToken        string           `json:"nextToken"`
	ConnectorContext ConnectorContext `json:"connectorContext"`
}

type ListEntitiesResponse struct {
	IsSuccess    bool          `json:"isSuccess"`
	ErrorDetails *ErrorDetails `json:"errorDetails,omitempty"`
	Entities     *[]Entity     `json:"entities"`
	NextToken    string        `json:"nextToken"`
	CacheControl *CacheControl `json:"cacheControl,omitempty"`
}

type CacheControl struct {
	TimeToLive     int    `json:"timeToLive"`
	TimeToLiveUnit string `json:"timeToLiveUnit"`
}

type DescribeEntityRequest struct {
	Type             string           `json:"type"`
	EntityIdentifier string           `json:"entityIdentifier"`
	ConnectorContext ConnectorContext `json:"connectorContext"`
}

type DescribeEntityResponse struct {
	IsSuccess        bool              `json:"isSuccess"`
	ErrorDetails     *ErrorDetails     `json:"errorDetails,omitempty"`
	EntityDefinition *EntityDefinition `json:"entityDefinition"`
	CacheControl     *CacheControl     `json:"cacheControl,omitempty"`
}

type RetrieveDataRequest struct {
	Type               string           `json:"type"`
	EntityIdentifier   string           `json:"entityIdentifier"`
	ConnectorContext   ConnectorContext `json:"connectorContext"`
	SelectedFieldNames []string         `json:"selectedFieldNames"`
	IDFieldName        string           `json:"idFieldName"`
	Ids                []string         `json:"ids"`
}

type RetrieveDataResponse struct {
	IsSuccess    bool          `json:"isSuccess"`
	ErrorDetails *ErrorDetails `json:"errorDetails"`
	Records      []string      `json:"records"`
}

type WriteDataRequest struct {
	Type             string           `json:"type"`
	EntityIdentifier string           `json:"entityIdentifier"`
	Operation        string           `json:"operation"`
	ConnectorContext ConnectorContext `json:"connectorContext"`
	IDFieldNames     []string         `json:"idFieldNames"`
	Records          []string         `json:"records"`
	AllOrNone        bool             `json:"allOrNone"`
}

type WriteDataResponse struct {
	IsSuccess          bool                `json:"isSuccess"`
	ErrorDetails       *ErrorDetails       `json:"errorDetails"`
	WriteRecordResults *WriteRecordResults `json:"writeRecordResults"`
}

type WriteRecordResults struct {
	IsSuccess    bool   `json:"isSuccess"`
	RecordID     string `json:"recordId"`
	ErrorMessage string `json:"errorMessage"`
}
