package appflow

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
)

type Connector interface {
	DescribeConnectorConfiguration(request *DescribeConnectorConfigurationRequest) *DescribeConnectorConfigurationResponse
	ValidateConnectorRuntimeSettings(request *ValidateConnectorRuntimeSettingsRequest) *ValidateConnectorRuntimeSettingsResponse
	ValidateCredentials(request *ValidateCredentialsRequest) *ValidateCredentialsResponse
	QueryData(request *QueryDataRequest) *QueryDataResponse
	ListEntities(request *ListEntitiesRequest) *ListEntitiesResponse
	DescribeEntity(request *DescribeEntityRequest) *DescribeEntityResponse
	RetrieveData(request *RetrieveDataRequest) *RetrieveDataResponse
	WriteData(request *WriteDataRequest) *WriteDataResponse
}

type LambdaHandler struct {
	Connector
}

func (h *LambdaHandler) handleRequest(requestType string, payload []byte) ([]byte, error) {
	switch requestType {
	case "DescribeConnectorConfigurationRequest":
		request := &DescribeConnectorConfigurationRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into DescribeConnectorConfigurationRequest")
		}
		return json.Marshal(h.DescribeConnectorConfiguration(request))
	case "ValidateConnectorRuntimeSettingsRequest":
		request := &ValidateConnectorRuntimeSettingsRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into ValidateConnectorRuntimeSettingsRequest")
		}
		return json.Marshal(h.ValidateConnectorRuntimeSettings(request))
	case "ValidateCredentialsRequest":
		request := &ValidateCredentialsRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into ValidateCredentialsRequest")
		}
		return json.Marshal(h.ValidateCredentials(request))
	case "QueryDataRequest":
		request := &QueryDataRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into QueryDataRequest")
		}
		return json.Marshal(h.QueryData(request))
	case "ListEntitiesRequest":
		request := &ListEntitiesRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into ListEntitiesRequest")
		}
		return json.Marshal(h.ListEntities(request))
	case "DescribeEntityRequest":
		request := &DescribeEntityRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into DescribeEntityRequest")
		}
		response := h.DescribeEntity(request)
		return json.Marshal(response)
	case "RetrieveDataRequest":
		request := &RetrieveDataRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into RetrieveDataRequest")
		}
		return json.Marshal(h.RetrieveData(request))
	case "WriteDataRequest":
		request := &WriteDataRequest{}
		if err := json.Unmarshal(payload, request); err != nil {
			return nil, fmt.Errorf("unable to Unmarshal JSON into WriteDataRequest")
		}
		return json.Marshal(h.WriteData(request))
	default:
		return nil, fmt.Errorf("unknown request type %s", requestType)
	}
}

func (h *LambdaHandler) Invoke(ctx context.Context, payload []byte) ([]byte, error) {
	log.Printf("Request: %s\n", string(payload))
	var event map[string]interface{}
	if err := json.Unmarshal(payload, &event); err != nil {
		return nil, fmt.Errorf("unable to Unmarshal JSON")
	}
	eventType, ok := event["type"]
	if !ok {
		return nil, fmt.Errorf("event does not contain type field")
	}
	switch c := eventType.(type) {
	case string:
		ret, err := h.handleRequest(c, payload)
		if err != nil {
			return nil, fmt.Errorf("error handling event type %s: %v", c, err)
		}
		log.Printf("Response: %s\n", string(ret))
		return ret, nil
	default:
		return nil, fmt.Errorf("unknown request type %v", c)
	}
}
