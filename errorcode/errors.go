package errorcode

import "encoding/json"

type ErrorCode int

const (
	// Invalid arguments provided as input/HttpStatus 400/413 from application/Bad Request exception from Application.
	// For example QueryURI too large, write request payload too large etc.
	InvalidArgument ErrorCode = iota

	// Credentials were rejected by the underlying application/HttpStatus 401 from Application.
	InvalidCredentials

	// Resource access denied by the underlying application/HttpStatus 403 from Application.
	AccessDenied

	// The request to the underlying application timed out/HttpStatus 408 from Application/
	// HttpClient timeout while sending request.
	RequestTimeout

	// Request got rejected by the underlying application due to rate limit violation/HttpStatus 429 from Application.
	RateLimitExceeded

	// Application is not available to serve the requests at the moment/HttpStatus 503 from Application.
	ServiceUnavailable

	// Specifies error is due to client or HttpStatus 4XX from Application.
	// Use specific error codes if present.
	ClientError

	// Specifies error is due to Application or HttpStatus 5XX from Application.
	// Use specific error codes if present.
	ServerError

	// Unknown Error from the Application. Use this ErrorCode only when you are not able to use the
	// other specific error codes.
	UnknownError

	// Specifies that the connector encountered failure, for some records, while writing to the application.
	PartialWriteFailure

	// Specifies that the connector is unable to find resource like AWS SecretManagerARN etc.
	ResourceNotFoundError
)

func (t ErrorCode) String() string {
	return [...]string{"InvalidArgument", "InvalidCredentials", "AccessDenied",
		"RequestTimeout", "RateLimitExceeded", "ServiceUnavailable", "ClientError",
		"ServerError", "UnknownError", "PartialWriteFailure", "ResourceNotFoundError"}[t]
}

func (t *ErrorCode) FromString(kind string) ErrorCode {
	return map[string]ErrorCode{
		"InvalidArgument":       InvalidArgument,
		"InvalidCredentials":    InvalidCredentials,
		"AccessDenied":          AccessDenied,
		"RequestTimeout":        RequestTimeout,
		"RateLimitExceeded":     RateLimitExceeded,
		"ServiceUnavailable":    ServiceUnavailable,
		"ClientError":           ClientError,
		"ServerError":           ServerError,
		"UnknownError":          UnknownError,
		"PartialWriteFailure":   PartialWriteFailure,
		"ResourceNotFoundError": ResourceNotFoundError,
	}[kind]
}

func (t ErrorCode) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

func (t *ErrorCode) UnMarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	*t = t.FromString(s)
	return nil
}
